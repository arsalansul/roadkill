﻿using System.Collections.Generic;

namespace Assets.Scripts
{
    public static class Explosions
    {
        public static List<Explosion> ExplosionsList = new List<Explosion>();

        public static void ActivateAllAndClearList()
        {
            foreach (var explosion in ExplosionsList)
            {
                explosion.Activate();
            }

            ExplosionsList.Clear();
        }
    }
}
