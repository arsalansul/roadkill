﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    public static class ProgressSaver
    {
        private static readonly string levelKey = "Level";
        private static readonly string moneyKey = "Money";
        private static readonly string explosionUpgradeKey = "ExplosionUpgrade";
        private static readonly string collisionUpgradeKey = "CollisionUpgrade";
        private static readonly string offlineUpgradeKey = "OfflineUpgrade";
        private static readonly string damageUpgradeKey = "DamageUpgrade";
        private static readonly string lastActiveTimeKey = "LastActiveTime";
        private static readonly string fireTutorialKey = "FireTutorial";
        private static readonly string upgradeTutorialKey = "UpgradeTutorial";
        private static readonly string allMapKey = "Map";
        private static readonly string currentWaypointIndexFromTheEndKey = "MapIndexFromTheEnd";

        //map
        //4 bits for MapLoader index
        //save last 8 indexes

        private static readonly int mapLastIndexMask = (1<<4) - 1;
        public static void Save(Upgrades upgrades)
        {
            PlayerPrefs.SetInt(levelKey, Levels.Level);
            PlayerPrefs.SetInt(moneyKey, Settings.Instance.Money);
            PlayerPrefs.SetInt(explosionUpgradeKey, upgrades.GetExplosionUpgradeLevel());
            PlayerPrefs.SetInt(collisionUpgradeKey, upgrades.GetCollisionUpgradeLevel());
            PlayerPrefs.SetInt(offlineUpgradeKey, upgrades.GetOfflineUpgradeLevel());
            PlayerPrefs.SetInt(damageUpgradeKey, upgrades.GetDamageUpgradeLevel());

            int unixTime = (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
            PlayerPrefs.SetInt(lastActiveTimeKey, unixTime);
        }

        public static int GetLevel()
        {
            return PlayerPrefs.GetInt(levelKey, 1);
        }

        public static int GetMoney()
        {
            return PlayerPrefs.GetInt(moneyKey, 0);
        }

        public static int GetExplosionUpgradeLevel()
        {
            return PlayerPrefs.GetInt(explosionUpgradeKey, 1);
        }

        public static int GetCollisionUpgradeLevel()
        {
            return PlayerPrefs.GetInt(collisionUpgradeKey, 1);
        }

        public static int GetOfflineUpgradeLevel()
        {
            return PlayerPrefs.GetInt(offlineUpgradeKey, 1);
        }

        public static int GetDamageUpgradeLevel()
        {
            return PlayerPrefs.GetInt(damageUpgradeKey, 1);
        }

        public static int GetLastActiveTime()
        {
            return PlayerPrefs.GetInt(lastActiveTimeKey, 0);
        }

        public static void FireTutorialComplete()
        {
            PlayerPrefs.SetInt(fireTutorialKey, 1);
        }

        public static int GetFireTutorialStatus()
        {
            return PlayerPrefs.GetInt(fireTutorialKey, 0);
        }

        public static void UpgradeTutorialComplete()
        {
            PlayerPrefs.SetInt(upgradeTutorialKey, 1);
        }

        public static int GetUpgradeTutorialStatus()
        {
            return PlayerPrefs.GetInt(upgradeTutorialKey, 0);
        }

        public static void SaveMap(int index)
        {
            PlayerPrefs.SetInt(allMapKey, (GetAllMap() << 4) | index);
        }

        public static void SaveCurrentMapIndexFromTheEnd(int currentIndex)
        {
            PlayerPrefs.SetInt(currentWaypointIndexFromTheEndKey, currentIndex);
        }

        private static int GetAllMap()
        {
            return PlayerPrefs.GetInt(allMapKey, 0);
        }

        public static int[] GetMapIndexes()
        {
            var result = new int[8];
            var allMap = GetAllMap();

            for (int i = 7; i >=0; i--)
            {
                result[i] = allMap & mapLastIndexMask;
                allMap >>= 4;
            }

            return result;
        }

        public static int GetCurrentWaypointIndexFromTheEnd()
        {
            return PlayerPrefs.GetInt(currentWaypointIndexFromTheEndKey, 0);
        }

        public static void LoseProgress()
        {
            PlayerPrefs.SetInt(levelKey, 1);
            PlayerPrefs.SetInt(moneyKey, 0);
            PlayerPrefs.SetInt(explosionUpgradeKey, 1);
            PlayerPrefs.SetInt(collisionUpgradeKey, 1);
            PlayerPrefs.SetInt(offlineUpgradeKey, 1);
            PlayerPrefs.SetInt(damageUpgradeKey, 1);
            PlayerPrefs.SetInt(lastActiveTimeKey, 0);

            PlayerPrefs.SetInt(fireTutorialKey, 0);
            PlayerPrefs.SetInt(upgradeTutorialKey, 0);

            PlayerPrefs.SetInt(allMapKey, 0);
            PlayerPrefs.SetInt(currentWaypointIndexFromTheEndKey, 0);
        }
    }
}
