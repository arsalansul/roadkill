﻿using UnityEngine;

namespace Assets.Scripts
{
    public static class Levels
    {
        private static int level = 1;

        public static bool comlete;

        public static int Level
        {
            get => level;
            set
            {
                if (value < 1)
                {
                    level = 1;
                    return;
                }

                level = value;
            }
        }
    }
}
