﻿using UnityEngine;

namespace Assets.Scripts
{
    public class MainCamera
    {
        private Camera mainCamera;
        public Camera camera => mainCamera;

        public Vector3 targetPosition;
        public Quaternion targetRotation;

        private const float speed = 100;
        public MainCamera()
        {
            mainCamera = Camera.main;
        }

        private Vector3 Position
        {
            get => mainCamera.transform.position;
            set => mainCamera.transform.position = value;
        }

        private Quaternion Rotation
        {
            get => mainCamera.transform.rotation;
            set => mainCamera.transform.rotation = value;
        }

        public bool MoveToTarget()
        {
            bool result = false;
            if (Position != targetPosition)
            {
                Position = Vector3.MoveTowards(Position, targetPosition, Time.deltaTime * ((Position - targetPosition).magnitude + speed));
                result = true;
            }

            if (Rotation != targetRotation)
            {
                Rotation = Quaternion.RotateTowards(Rotation, targetRotation, Time.deltaTime * speed);
                result = true;
            }

            return result;
        }

    }
}
