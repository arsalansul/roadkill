﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Explosion
    {
        public Vector3 Position;
        public float Radius;
        private float Power;
        private Exploder exploder;
        private const int layerMask = 1<<9;

        private LineRenderer lineRenderer;
        public Explosion(Vector3 position, Exploder exploder)
        {
            Position = position;
            Radius = Settings.Instance.explosionSettings.Radius;
            this.exploder = exploder;
            Power = Settings.Instance.explosionSettings.Power;

            if (Settings.Instance.explosionSettings.ExplosionDebugRender)
            {
                lineRenderer = exploder.gameObject.AddComponent<LineRenderer>();
                lineRenderer.SetPosition(0, Position);
            }
        }

        public void Activate()
        {
            exploder.enabled = true;
            var hits = Physics.SphereCastAll(Position, Radius, Vector3.forward, 0, layerMask);

            if (Settings.Instance.explosionSettings.ExplosionDebugRender)
                lineRenderer.positionCount = hits.Length * 2 + 1;

            for (int i = 0; i < hits.Length; i++)
            {
                Vector3 direction = (hits[i].transform.position - Position);
                var force = (Radius - direction.magnitude) / Radius * Power;

                direction.Normalize();
                var rndV3 = new Vector3(Random.value, Random.value, Random.value).normalized;
                hits[i].transform.GetComponent<Rigidbody>().AddForceAtPosition(direction * force, hits[i].transform.position + rndV3, ForceMode.Impulse);
                hits[i].transform.GetComponent<Rigidbody>().useGravity = true;
                hits[i].transform.GetComponent<UnitMonoBehavior>().ExplosionAffected();

                if (Settings.Instance.explosionSettings.ExplosionDebugRender)
                {
                    lineRenderer.SetPosition(i * 2 + 1, hits[i].transform.position);
                    lineRenderer.SetPosition(i * 2 + 2, Position);

                    var drawer = hits[i].transform.gameObject.AddComponent<ForceDrawer>();
                    drawer.point = drawer.transform.InverseTransformPoint(hits[i].transform.position);
                    drawer.force = drawer.transform.InverseTransformDirection(direction * force / 5);

                    Debug.Log("Apply force to " + hits[i].transform + " " + force, hits[i].transform);
                }
            }
        }

        public override string ToString()
        {
            return Position + " " + Radius + " " + Power;
        }
    }
}
