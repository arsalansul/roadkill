﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXLibrary : MonoBehaviour {

	public static SFXLibrary instance;

    //public AudioClip btnClick;
    //
    //public AudioClip portalOpen;
    //
    //public AudioClip showPlayerCard;
    //
    //public AudioClip clock;
    //
    //public AudioClip jumpUp;
    //
    //public AudioClip jumpDown;
    //
    //public AudioClip loseRound;
    //
    //public AudioClip winRound;
    //
    //public AudioClip coin;//play after winRound
    //
    //public AudioClip coins;
    //
    //public AudioClip startBattle;
    //
    //public AudioClip minionLevelUp;
    //
    //public AudioClip gameOver;
    //
    //public AudioClip choose;
    //
    //public AudioClip buy;
    //
    //public AudioClip roll;
    //
    //public AudioClip minionDamage;

    public AudioClip[] explosionClip;
    public AudioClip[] crashClip;
    public AudioClip[] bibipClip;


    void Awake () {
	
		if (instance != null) {
			Destroy (gameObject);
			return;
		}

		instance = this;
	}
}
