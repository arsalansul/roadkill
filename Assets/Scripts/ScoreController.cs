﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public static class ScoreController
    {
        public static int Score { get; private set; }
        public static int Damage { get; private set; }

        private const int ScoreForExplosion = 100;
        
        public static bool scoreChanged;
        public static int explosionsCount = 0;
        public static int collisionsCount = 0;

        private static List<Unit> explodedUnits = new List<Unit>();

        public delegate void ExplosionBonusText(int bonusText, Vector3 positionWorld);
        public static ExplosionBonusText explosionText;
        private static void AddScore(int addScore)
        {
            if (addScore <= 0)
                return;

            Score += addScore;
            scoreChanged = true;
        }

        private static void AddDamage(int addDamage) //pure damage
        {
            Damage += addDamage;
        }

        public static void AddScoreForExplosion(Unit unit)
        {
            var explosionBonus = (int) (ScoreForExplosion * Settings.scoreMultiplierExplosion);
            AddScore(explosionBonus);
            explosionsCount++;
            explodedUnits.Add(unit);
            explosionText(explosionBonus, unit.Position);
        }

        public static void AddScoreForCollision(int forceMagnitude)
        {
            AddScore((int)(forceMagnitude * Settings.scoreMultiplierCollision));
            AddDamage((int)(forceMagnitude/Settings.damageMultiplier));
            collisionsCount++;
        }

        public static void ResetScore()
        {
            Score = 0;
            explosionsCount = 0;
            collisionsCount = 0;
        }

        public static void ResetDamage()
        {
            Damage = 0;
        }

        public static Unit FindExplodedUnitByName(string name)
        {
            foreach (var unit in explodedUnits)
            {
                if (unit.Name == name)
                {
                    return unit;
                }
            }

            return null;
        }

        public static void ClearExplodedUnitsList()
        {
            explodedUnits.Clear();
        }

        public static void RemoveUnitFromExplodedList(Unit unit)
        {
            explodedUnits.Remove(unit);
        }
    }
}
