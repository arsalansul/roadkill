﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Waypoint
    {
        public Vector3 PosV3;

        public Vector2 PosV2 => new Vector2(PosV3.x, PosV3.z);

        public int index;

        private bool isFree;
        public bool IsFree
        {
            get => isFree;
            set
            {
                if (alwaysBusy && isFree == false)
                    return;

                isFree = value;
            }
        }

        public bool canMove;

        public bool alwaysBusy;
        public static float minDistToFree = 3;
    }
}
