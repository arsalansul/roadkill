﻿using UnityEngine;

namespace Assets.Scripts
{
    [ExecuteAlways]
    public class WaypointsParentEditor : MonoBehaviour
    {
        public bool AddWaypointsParent;

        void Update()
        {
            if (!AddWaypointsParent)
            {
                return;
            }

            var parent = new GameObject("waypointsParent");
            parent.transform.SetParent(transform);
            parent.AddComponent<WaypointsEditor>();

            AddWaypointsParent = false;
        }
    }
}
