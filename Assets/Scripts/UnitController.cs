﻿using UnityEngine;

namespace Assets.Scripts
{
    public class UnitController
    {
        private ObjectPooler objectPooler;
        public UnitController(ObjectPooler objectPooler)
        {
            this.objectPooler = objectPooler;
        }

        public void MoveUnitsAndRotateWheels()
        {
            foreach (var unit in objectPooler.units)
            {
                if (unit.IsFree())
                    continue;

                if (unit.CloseToThePoint(unit.targetPoint))
                {
                    unit.onReached?.Invoke(unit, unit.targetPoint);
                }

                unit.Move();
                unit.Rotate();
            }
        }

        public void ActivateRigidbodyAroundExplosion(Explosion explosion)
        {
            foreach (var unit in objectPooler.units)
            {
                if (unit.IsFree())
                    continue;

                var dist = (unit.Position - explosion.Position).magnitude;

                if (dist < explosion.Radius)
                {
                    unit.MoveByRigidbody();
                }
            }
        }

        public float GetMinDistToPoint(Vector3 point)
        {
            float result = System.Single.PositiveInfinity;
            foreach (var unit in objectPooler.units)
            {
                if (unit.IsFree())
                    continue;

                var dist = (unit.Position - point).magnitude;
                if (dist < result)
                    result = dist;
            }

            return result;
        }
    }
}
