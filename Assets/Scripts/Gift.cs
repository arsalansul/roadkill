﻿using System;

namespace Assets.Scripts
{
    public class Gift
    {
        public delegate void GiftAction();

        public GiftAction giftAction;

        public Gift(GiftAction giftAction)
        {
            this.giftAction = giftAction;
        }

        public void Show()
        {
            giftAction();
        }
    }
}
