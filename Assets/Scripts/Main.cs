﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    public class Main : MonoBehaviour
    {
        private Transform[] waypointsParents;

        private UnitController unitController;

        private  WaypointsManager[][] waypointsManagers;
        private  List<WaypointsManager> waypointsManagersActive;

        private ObjectPooler objectPooler;

        private float timer = 0;
        private float timerLastActivate = 0;
        private float timeToActivateUnit;
        private const float timerSpawnAfterShoot = 15;
        private const float timerAfterLastScoreChanged = 3;
        private float shootTime;
        private float lastScoreChangedTime;

        private const int unitsCountInPool = 5;

        private Transform playerParent;
        private Unit player;

        private bool fire;
        private Arrow arrow;

        private MainCamera mainCamera;

        private UIController uiController;

        private ChallengeController challengeController;
        private Challenge challenge;

        private bool end;

        private Transform Clouds;

        private Transform ExplodableBuildingsParent;

        private GameObject[] LevelGOs;
        private GameObject currentLevelGO;

        private MapLoader mapLoader;

        private Upgrades upgrades;

        private GiftManager giftManager;

        void Start()
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;

            Settings.Instance.Money = ProgressSaver.GetMoney();
            Levels.Level = ProgressSaver.GetLevel();

            upgrades = new Upgrades(
                ProgressSaver.GetExplosionUpgradeLevel(),
                ProgressSaver.GetCollisionUpgradeLevel(), 
                ProgressSaver.GetDamageUpgradeLevel(), 
                ProgressSaver.GetOfflineUpgradeLevel()
                );
            
            uiController = new UIController(upgrades);
            challengeController = new ChallengeController();
            mainCamera = new MainCamera();
            mapLoader = new MapLoader(mainCamera.camera);
            objectPooler = new ObjectPooler();
            objectPooler.CreateUnitsPool(unitsCountInPool);
            unitController = new UnitController(objectPooler);
            waypointsManagersActive = new List<WaypointsManager>();
            giftManager = new GiftManager(upgrades, uiController);

            mapLoader.LoadStartMap(out var allMap);
            if (allMap.Count > 0)
            {
                GenerateWaypointManagers(allMap);
            }
            else
            {
                mapLoader.LoadMap(out var map);
                GenerateWaypointManagers(map);
            }

            Clouds = GameObject.Find("Clouds").transform;
            Clouds.transform.position = new Vector3(Clouds.transform.position.x, Clouds.transform.position.y, Random.Range(-100, 100));

            LoadLevel();

            var currentTime = (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;

            if (ProgressSaver.GetLastActiveTime() != 0)
            {
                var reward = new Reward( (int)(Mathf.Min(currentTime - ProgressSaver.GetLastActiveTime(), Settings.MaxScoreOffline) * Settings.scoreMultiplierOffline), "Thanks for come back");
                uiController.ShowReward(reward, Color.white);
            }

            AdsController.Instance().HideBanner();
        }

        void Update()
        {
            if (Input.GetKeyUp(KeyCode.G))
            {
                giftManager.CheckForGifts();
            }

            if (mainCamera.MoveToTarget())
            {
                mapLoader.needCheckMaps = true;
            }
            else if (mapLoader.needCheckMaps && mapLoader.CheckMapsToRenderOnScreen())
            {
                mapLoader.LoadMap(out var map);
                GenerateWaypointManagers(map);
            }

            if (Settings.uiState != UIStates.Game || end)
                return;

            if (challengeController.CheckChanges(challenge))
            {
                uiController.SetChallengeValues(challenge);
            }

            if (challenge.complete && !Levels.comlete)
            {
                Levels.comlete = true;
                uiController.NextLevelWindow_show();
            }

            if (ScoreController.scoreChanged)
            {
                lastScoreChangedTime = timer;
                ScoreController.scoreChanged = false;
            }

            foreach (var explosion in Explosions.ExplosionsList)
            {
                unitController.ActivateRigidbodyAroundExplosion(explosion);
            }

            unitController.MoveUnitsAndRotateWheels();

            Explosions.ActivateAllAndClearList();

            if (fire)
            {
                player.Move();

                if (timer > lastScoreChangedTime + timerAfterLastScoreChanged)
                {
                    if (Settings.uiState == UIStates.Game)
                    {
                        string rewardDescr;
                        if (challenge.complete)
                        {
                            rewardDescr = "Congratulation!";
                            Levels.Level++;
                        }
                        else
                            rewardDescr = "Try again";

                        uiController.ShowReward(new Reward(ScoreController.Score, rewardDescr), LoadLevel);
                        end = true;
                    }
                    return;
                }

                if (timer > shootTime + timerSpawnAfterShoot)
                {
                    timer += Time.deltaTime;
                    return;
                }
            }

            if (timer - timerLastActivate > timeToActivateUnit )
            {
                timerLastActivate = timer;
                UpdateStartWaypointsStatus();
                foreach (var wm in waypointsManagersActive)
                {
                    objectPooler.ActivateRandomUnit(wm.GetStart(), wm.GetDirectionToNextWaypoint(wm.GetStart()),
                        wm.OnReached, wm.Level);
                }

            }

            arrow.Rotate();

            timer += Time.deltaTime;
        }

        public void Fire()
        {
            player.LocalRotation = arrow.GetRotation();
            player.MoveByRigidbody();
            arrow.Disable();

            shootTime = timer;
            lastScoreChangedTime = timer;
            fire = true;

            uiController.DisableButtons();
        }

        private void LoadLevel()
        {
            Settings.uiState = UIStates.Game;
            end = false;
            fire = false;
            timer = 0;
            timerLastActivate = 0;
            timeToActivateUnit = 0;
            shootTime = 0;
            lastScoreChangedTime = 0;

            if (objectPooler != null)
            {
                objectPooler.ReturnAllUnitsToPool();
            }

            NewCondition();
        }

        private void UpdateStartWaypointsStatus()
        {
            for (int i = 0; i < waypointsManagers.Length; i++)
            {
                for (int j = 0; j < waypointsManagers[i].Length; j++)
                {
                    var start = waypointsManagers[i][j].GetStart();
                    if (unitController.GetMinDistToPoint(start.PosV3) < Waypoint.minDistToFree)
                    {
                        start.IsFree = false;
                        continue;
                    }

                    start.IsFree = true;
                }
            }
        }

        private void NewCondition()
        {
            if (player != null)
            {
                player.Destroy();
                player = null;
            }
            
            challenge = challengeController.GetChallenge(Levels.Level);
            challenge.Reset();

            uiController.SetStartChallengeValues(challenge);

            uiController.UpdateUI();

            GetActiveWaypoints();

            for (int i = 0; i < waypointsManagers.Length; i++)
            {
                for (int j = 0; j < waypointsManagers[i].Length; j++)
                {
                    waypointsManagers[i][j].ResetWaipoints();

                    if (waypointsManagers[i][j].Level == Levels.Level)
                    {
                        playerParent = waypointsManagers[i][j].waypointsParent.parent.parent.Find("PlayerSpawnPoint");
                    }
                }
            }

            player = new Unit(Resources.Load<GameObject>("Prefabs/Player"));
            player.Speed = 50;
            player.CreateUnitGO(playerParent, true, false);

            arrow = new Arrow(player.FindChild("ArrowDummy"));

            timeToActivateUnit = Settings.Instance.unitSettings.RangeToSpawn / Settings.Instance.unitSettings.Speed;

            mainCamera.targetPosition = playerParent.parent.Find("CameraPosition").position;
            mainCamera.targetRotation = playerParent.parent.Find("CameraPosition").rotation;

            Clouds.transform.position = new Vector3(mainCamera.targetPosition.x, Clouds.transform.position.y, Clouds.transform.position.z);

            Levels.comlete = false;
            ScoreController.ResetScore();
            ScoreController.ResetDamage();
            ScoreController.ClearExplodedUnitsList();
            Explosions.ExplosionsList.Clear();
            Settings.Instance.unitSettings.HealthMultiplier = 1 + Levels.Level * Levels.Level / 50f;
            Settings.scoreMultiplierCollisionHelper = false;

            giftManager.CheckForGifts();
        }

        private void GenerateWaypointManagers(GameObject newMap)
        {
            if (newMap == null)
                return;

            var oldWM = waypointsManagers;
            LevelGOs = GameObject.FindGameObjectsWithTag("Levels");
            var newLevelGOs = LevelGOs.Where(go => go.transform.parent == newMap.transform).OrderBy(go => go.name);

            waypointsParents = new Transform[LevelGOs.Length];

            var level = waypointsManagers == null ? 0 : waypointsManagers[waypointsManagers.Length - 1][0].Level;

            waypointsManagers = new WaypointsManager[waypointsParents.Length][];

            var index = oldWM == null ? 0 : waypointsManagers.Length - newLevelGOs.Count();
            for (int i = index-1, j = oldWM == null ? 0 : oldWM.Length - 1; i >= 0 && j >=0; i--, j--)
            {
                waypointsManagers[i] = oldWM[j];
            }

            for (int i = index; i < waypointsManagers.Length; i++)
            {
                level++;
                
                waypointsParents[i] = newLevelGOs.ElementAt(i - index).transform.Find("WaypointsParent");
                waypointsManagers[i] = new WaypointsManager[waypointsParents[i].childCount];
                for (int j = 0; j < waypointsParents[i].childCount; j++)
                {
                    waypointsManagers[i][j] = new WaypointsManager(waypointsParents[i].GetChild(j), level);
                }
            }

            GetActiveWaypoints();
        }

        private void GetActiveWaypoints()
        {
            waypointsManagersActive.Clear();
            for (int i = 0; i < waypointsManagers.Length; i++)
            {
                foreach (var wm in waypointsManagers[i])
                {
                    if (wm.Level >= Levels.Level && wm.Level < Levels.Level + 3)
                        waypointsManagersActive.Add(wm);

                    if (wm.Level == Levels.Level)
                        ProgressSaver.SaveCurrentMapIndexFromTheEnd(waypointsManagers.Length - i);
                }
            }

            if (waypointsManagersActive.Count == 0)
            {
                mapLoader.LoadMap(out var map);
                GenerateWaypointManagers(map);
            }
        }

        private void GenerateWaypointManagers(List<GameObject> allMap)
        {
            LevelGOs = GameObject.FindGameObjectsWithTag("Levels");

            waypointsParents = new Transform[LevelGOs.Length];
            waypointsManagers = new WaypointsManager[waypointsParents.Length][];

            var index = waypointsParents.Length - ProgressSaver.GetCurrentWaypointIndexFromTheEnd();
            var level = Levels.Level - index - 1;

            for (int i = 0; i < waypointsManagers.Length; i++)
            {
                level++;
                waypointsParents[i] = LevelGOs[i].transform.Find("WaypointsParent");
                waypointsManagers[i] = new WaypointsManager[waypointsParents[i].childCount];
                for (int j = 0; j < waypointsParents[i].childCount; j++)
                {
                    waypointsManagers[i][j] = new WaypointsManager(waypointsParents[i].GetChild(j), level);
                }
            }

            GetActiveWaypoints();
        }
    }
}
