﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public class StartMenu : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        public void PlayButton()
        {
            Settings.uiState = UIStates.LevelSelect;
            SceneManager.LoadScene("Game");
        }
    }
}
