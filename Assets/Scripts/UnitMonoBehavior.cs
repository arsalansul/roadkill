﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts
{
    public class UnitMonoBehavior : MonoBehaviour
    {
        public System.Action rigidbodyActivate;
        public System.Action<int> addScore;
        public System.Action explosionActivate;

        public int[] levels;
        public bool andGreater;

        public bool triggered;

        private const int layerMaskIgnoreRaycast = 2;

        public int startHealth = 100;
        private int health;
        private int healthOnCompleteLevel;
        private int startLevelHealth;

        public int Health
        {
            get => Levels.comlete ? healthOnCompleteLevel : health;
            set
            {
                health = value;
                if (!Levels.comlete)
                    healthOnCompleteLevel = value * 10;
            }
        }

        private Material[] materials;
        private byte colorComponent = 255;

        private const float minRelativeVelocityMagnToPlayCrashSound = 12;
        private const float maxRelativeVelocityMagnToPlayBibipSound = 1;

        private List<Collision> collisions = new List<Collision>();
        private List<float> velocities = new List<float>();

        private bool explosionAffected;
        private int reduceDamageForNTimesMax = 4;
        private int reduceDamageForNTimesMin = 2;
        private int reduceDamageCounter;

        void Update()
        {
            if (exploded)
                return;

            for (int i = 0; i < collisions.Count; i++)
            {
                if (collisions[i] == null || collisions[i].contactCount == 0 || collisions[i].contacts[0].thisCollider.transform != transform)
                {
                    collisions.RemoveAt(i);
                    velocities.RemoveAt(i);
                    i--;
                    continue;
                }

                float vel = collisions[i].relativeVelocity.magnitude;

                if (vel > velocities[i])
                {
                    ApplyDamage(vel - velocities[i]);
                }

                velocities[i] = vel;
            }
        }

        private void ApplyDamage(float damage)
        {
            if (damage < 1f)
                return;

            var totalDamage = damage;

            if (!explosionAffected)
                totalDamage *= Settings.damageMultiplier;
            else if(--reduceDamageCounter <= 0)
            {
                explosionAffected = false;
            }

            addScore(Mathf.Min((int)damage, startLevelHealth));
            Health -= (int)totalDamage;

            if (!Levels.comlete)
                colorComponent = (byte)Mathf.Clamp(127 + 128 * Health / startLevelHealth, 128, 255);

            foreach (var material in materials)
            {
                material.color = new Color32(colorComponent, colorComponent, colorComponent, 255);
            }

            if (Health < 0 && !exploded)
            {
                explosionActivate();
                foreach (var material in materials)
                {
                    material.color = Color.black;
                }

                exploded = true;

                SFXPlayer.Instance().Play(SFXLibrary.instance.explosionClip[Random.Range(0, SFXLibrary.instance.explosionClip.Length)],false,false,0.13f);
            }
        }

        private void Start()
        {
            startLevelHealth = (int) (startHealth * Settings.Instance.unitSettings.HealthMultiplier);
            Health = startLevelHealth;

            var meshR = gameObject.GetComponentsInChildren<Renderer>();
            if (meshR != null)
            {
                materials = new Material[meshR.Length];
                for (int i = 0; i < meshR.Length; i++)
                {
                    materials[i] = meshR[i].material;
                }
            }
        }

        private bool exploded;
        private void OnCollisionEnter(Collision collision)
        {
            if (!collisions.Contains(collision))
            {
                collisions.Add(collision);
                velocities.Add(collision.relativeVelocity.magnitude);
            }

            var otherGo = collision.gameObject;
            var rb = otherGo.GetComponent<Rigidbody>();

            if (rb != null )
            {
                if (!triggered)
                    rigidbodyActivate();
            }

            if (!exploded && otherGo.layer != layerMaskIgnoreRaycast) //&& collision.relativeVelocity.magnitude > velocityToExplode)
            {
                ApplyDamage(collision.relativeVelocity.magnitude);

                if (collision.relativeVelocity.magnitude > minRelativeVelocityMagnToPlayCrashSound)
                    SFXPlayer.Instance().Play(SFXLibrary.instance.crashClip[Random.Range(0, SFXLibrary.instance.crashClip.Length)], false,false, 0.13f);
                else if (collision.relativeVelocity.magnitude < maxRelativeVelocityMagnToPlayBibipSound)
                {
                    SFXPlayer.Instance().Play(SFXLibrary.instance.bibipClip[Random.Range(0, SFXLibrary.instance.bibipClip.Length)], false, false, 0.13f);
                }
            }
        }

        private void OnCollisionStay(Collision collision)
        {
            if (!collisions.Contains(collision))
            {
                collisions.Add(collision);
                velocities.Add(collision.relativeVelocity.magnitude);
            }
        }

        public void Reset()
        {
            startLevelHealth = (int)(startHealth * Settings.Instance.unitSettings.HealthMultiplier);
            Health = startLevelHealth;
            exploded = false;
            explosionAffected = false;
        }

        public void ExplosionAffected()
        {
            reduceDamageCounter = Random.Range(reduceDamageForNTimesMin, reduceDamageForNTimesMax + 1);
            explosionAffected = true;
        }
    }
}