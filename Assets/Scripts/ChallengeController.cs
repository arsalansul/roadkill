﻿using UnityEngine;

namespace Assets.Scripts
{
    public class ChallengeController
    {
        private int allExplosions;

        public bool CheckChanges(Challenge challenge)
        {
            //if (challenge.complete)
            //    return false;

            if (challenge.explosionsCount != 0)
            {
                if (challenge.explodedCount >= challenge.explosionsCount)
                {
                    challenge.complete = true;
                    return true;
                }

                if (challenge.TargetName == null && challenge.explodedCount < ScoreController.explosionsCount)
                {
                    challenge.explodedCount++;
                    return true;
                }

                var explodedUnit = ScoreController.FindExplodedUnitByName(challenge.TargetName);
                bool result = false;
                while (explodedUnit != null)
                {
                    challenge.explodedCount++;
                    ScoreController.RemoveUnitFromExplodedList(explodedUnit);
                    explodedUnit = ScoreController.FindExplodedUnitByName(challenge.TargetName);
                    result = true;
                }

                return result;
            }

            if (challenge.causeDamage != 0)
            {
                if (challenge.causedDamage >= challenge.causeDamage)
                {
                    challenge.complete = true;
                    return true;
                }

                if (challenge.causedDamage < ScoreController.Score)
                {
                    challenge.causedDamage = ScoreController.Score;
                    return true;
                }
            }
            return false;
        }

        public Challenge GetChallenge(int level)
        {
            //int ind;
            //if (level <= Settings.Instance.challenges.Length)
            //    ind = level - 1;
            //else
            //{
            //    ind = Settings.Instance.challenges.Length - 1;
            //}

            return new Challenge
            {
                complete = false,
                //causeDamage = Settings.Instance.challenges[ind].causeDamage
                causeDamage = GetCauseDamageForChallenge(level)
            };
        }

        private int GetCauseDamageForChallenge(int level)
        {
            return level * (level - 1 )  * 1000 + 1000;
        }
    }
}
