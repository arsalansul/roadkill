﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Helicopter : Unit
    {
        private const int height = 10;

        private Wheel[] Rotors;
        public Helicopter(GameObject prefab) : base(prefab)
        {
        }

        public void CreateGO(Vector3 startPosition, Quaternion startRotation, bool active)
        {
            CreateUnitGO(startPosition, startRotation, active);
            unitMonoBehavior.rigidbodyActivate += RigidbodyActivate;
            unitMonoBehavior.explosionActivate += Exploded;
            unitMonoBehavior.addScore += AddScore;

            var rotors = unitGameObject.transform.Find("Rotors");
            if (rotors != null)
            {
                Rotors = new Wheel[rotors.childCount];
                for (int i = 0; i < rotors.childCount; i++)
                {
                    Rotors[i] = new Wheel(rotors.GetChild(i).gameObject);
                }
            }
        }

        private void Exploded()
        {
            ScoreController.AddScoreForExplosion(this);
        }

        protected override void RigidbodyActivate()
        {
            rigidbody.useGravity = true;
        }

        protected override void AddScore(int score)
        {
            ScoreController.AddScoreForCollision(score);
        }

        public override void Activate(Vector3 directionToNextWaypoint)
        {
            Position = targetPoint.PosV3 + new Vector3(0, height, 0);
            Rotation = Quaternion.LookRotation(directionToNextWaypoint);
            Speed = Settings.Instance.unitSettings.Speed;
            unitGameObject.SetActive(true);
        }

        protected override void RotateWheels()
        {
            if (Rotors == null)
                return;

            Rotors[0].Rotate(new Vector3(0, Speed,0));
            for (int i = 1; i < Rotors.Length; i++)
            {
                Rotors[i].Rotate(Speed);
            }
        }

        public override void ReturnToPool()
        {
            unitMonoBehavior.triggered = false;
            
            Position = new Vector3(1000, 1000, 1000);
            unitGameObject.GetComponent<Exploder>().enabled = false;

            rigidbody.useGravity = false;
            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = new Vector3(0f, 0f, 0f);

            unitGameObject.GetComponentInChildren<Renderer>().material.color = Color.white;

            unitGameObject.SetActive(false);
        }
    }
}
