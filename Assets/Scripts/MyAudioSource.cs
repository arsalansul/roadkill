﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyAudioSource : MonoBehaviour {

	public float volume {
		get {
			if (!inited)
				Init ();
			return player.volume;
		}
		set {
			if (!inited)
				Init ();
			player.volume = value;
		}
	}

	public AudioClip clip {
		get {
			if (!inited)
				Init ();
			return player.clip;
		}
		set {
			if (!inited)
				Init ();
			player.clip = value;
		}
	}

	public bool isPlaying {
		get {
			if (!inited)
				Init ();
			return player.isPlaying;
		}
		private set { }
	}

	public bool loop {
		get {
			if (!inited)
				Init ();
			return player.loop;
		}
		set {
			if (!inited)
				Init ();
			player.loop = value;
		}
	}

    /*public float maxDistance
    {
        get
        {
            if (!inited)
                Init();
            return player.maxDistance;
        }
        set
        {
            if (!inited)
                Init();
            player.maxDistance = value;
            player.spatialBlend = 1;
            player.rolloffMode = AudioRolloffMode.Linear;
        }

    }//*/

    private AudioSource player;
	
		private bool inited;
	private void Init () {
	
		player = gameObject.AddComponent<AudioSource> ();
        //player.spatialBlend = 1;
        //player.rolloffMode = AudioRolloffMode.Linear;
        //player.maxDistance = 20;
		inited = true;
	}

	void Awake () {

		Init ();
	}

	public void Play () {
	
		player.Play ();
	}

	public void Stop () {
	
		player.Stop ();
	}
}
