﻿using UnityEngine;

namespace Assets.Scripts
{
    public class ExplodableBuildings : Unit
    {
        private TrafficLight trafficLight;
        
        public ExplodableBuildings(GameObject prefab, TrafficLight tl) : base(prefab)
        {
            trafficLight = tl;
        }

        public ExplodableBuildings(GameObject go) : base(go)
        {
            unitMonoBehavior = unitGameObject.GetComponent<UnitMonoBehavior>();
            unitMonoBehavior.explosionActivate += Explode;
            unitMonoBehavior.explosionActivate += Exploded;
        }

        public void Create(Transform parent)
        {
            CreateUnitGO(parent, true, true);
            unitMonoBehavior.explosionActivate += TurnTrafficLight;
        }

        private void TurnTrafficLight()
        {
            trafficLight.SetLight(TrafficLight.LightColor.Green);
        }

        private void Exploded()
        {
            //ScoreController.AddScoreForExplosion(this);
        }

        public void Reset()
        {
            unitMonoBehavior.triggered = true;
            unitMonoBehavior.Reset();

            unitGameObject.GetComponent<Exploder>().enabled = false;
            unitGameObject.GetComponent<Exploder>().Reset();
            unitGameObject.GetComponentInChildren<Renderer>().material.color = Color.white;
        }
    }
}
