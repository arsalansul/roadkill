﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Reward
    {
        public string description { get; }
        public int amount { get; }

        private bool claimed = false;

        public Reward(int amount, string description)
        {
            this.amount = amount;
            this.description = description;
        }

        public void Claim()
        {
            if (claimed)
            {
                Debug.Log("Reward already claimed");
                return;
            }

            Settings.Instance.Money += amount;
            claimed = true;
        }

        public void ClaimForAd()
        {
            if (claimed)
            {
                Debug.Log("Reward already claimed");
                return;
            }

            Settings.Instance.Money += amount*GetMultForAD();
            claimed = true;
        }

        public override string ToString()
        {
            return description + "\n$" + amount;
        }

        public static int GetMultForAD()
        {
            return Mathf.Max(11 - Levels.Level,3);
        }
    }
}
