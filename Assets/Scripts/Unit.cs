﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Unit
    {
        public System.Action<Unit, Waypoint> onReached;
        public Waypoint targetPoint;
        public Waypoint StartWaypoint;

        protected UnitMonoBehavior unitMonoBehavior;

        protected GameObject unitGameObject;
        public string Name { get; private set; }

        private float speed = Settings.Instance.unitSettings.Speed;

        private const float turnSpeed = 5;
        private const float maxSpeed = 100;
        private const float minSpeed = 0;
        private const float minDistanceToChangeTarget = 5f;

        protected Rigidbody rigidbody;

        private Wheel[] wheels;

        public Unit(GameObject prefab)
        {
            unitGameObject = prefab;
            Name = prefab.name;
        }

        public float Speed
        {
            get => speed;
            set
            {
                if ((value >= minSpeed) && (value <= maxSpeed))
                {
                    speed = value;
                }
                else if (value < minSpeed)
                {
                    speed = minSpeed;
                }
                else
                {
                    speed = maxSpeed;
                }
            }
        }
        
        public Vector3 Position
        {
            get => unitGameObject.transform.position;
            protected set => unitGameObject.transform.position = value;
        }

        public Vector2 PositionV2
        {
            get => new Vector2(unitGameObject.transform.position.x, unitGameObject.transform.position.z);
        }

        public Quaternion Rotation
        {
            get => unitGameObject.transform.rotation;
            set => unitGameObject.transform.rotation = value;
        }

        public Quaternion LocalRotation
        {
            get => unitGameObject.transform.localRotation;
            set => unitGameObject.transform.localRotation = value;
        }
        
        public void CreateUnitGO()
        {
            unitGameObject = Object.Instantiate(unitGameObject);
            rigidbody = unitGameObject.GetComponent<Rigidbody>();

            //startSpeed = Speed;
            unitMonoBehavior = unitGameObject.GetComponent<UnitMonoBehavior>();
            unitMonoBehavior.rigidbodyActivate += MoveByRigidbody;
            unitMonoBehavior.explosionActivate += Explode;
            unitMonoBehavior.addScore += AddScore;

            var wheelsParent = unitGameObject.transform.Find("Wheels");
            if (wheelsParent != null)
            {
                wheels = new Wheel[wheelsParent.childCount];
                for (int i = 0; i < wheelsParent.childCount; i++)
                {
                    wheels[i] = new Wheel(wheelsParent.GetChild(i).gameObject);
                }
            }
        }

        public void CreateUnitGO(Vector3 startPosition, Quaternion startRotation, bool active)
        {
            CreateUnitGO();
            
            unitGameObject.transform.position = startPosition;
            unitGameObject.transform.rotation = startRotation;
            unitGameObject.SetActive(active);
        }

        public void CreateUnitGO(Transform parent, bool active, bool triggered)
        {
            CreateUnitGO();
            unitGameObject.transform.SetParent(parent);
            unitGameObject.transform.localPosition = Vector3.zero;
            unitGameObject.transform.localRotation = Quaternion.identity;
            unitGameObject.SetActive(active);

            unitMonoBehavior.triggered = triggered;
        }

        public void Move()
        {
            if (!unitMonoBehavior.triggered && targetPoint.canMove)
            {
                unitGameObject.transform.position = Vector3.MoveTowards(unitGameObject.transform.position, unitGameObject.transform.position + unitGameObject.transform.forward, Time.deltaTime * Speed);
            }
            else if (unitMonoBehavior.triggered)
            {
                //MoveByRigidbody();
                ChangeSpeed();
            }

            RotateWheels();
        }

        public void MoveByRigidbody()
        {
            if (unitMonoBehavior.triggered)
                return;

            rigidbody.velocity = unitGameObject.transform.forward * Speed;
            unitMonoBehavior.triggered = true;
            unitGameObject.GetComponent<Rigidbody>().useGravity = true;
        }

        public void MoveByRigidbody(Vector3 force)
        {
            if (unitMonoBehavior.triggered)
                return;

            rigidbody.velocity = unitGameObject.transform.forward * Speed;
            unitMonoBehavior.triggered = true;
            rigidbody.AddForce(force * 10); //magic force factor
        }

        public bool IsFree()
        {
            return !unitGameObject.activeInHierarchy;
        }

        public bool CloseToThePoint(Waypoint point)
        {
            return (PositionV2 - point.PosV2).magnitude < minDistanceToChangeTarget;
        }

        public virtual void ReturnToPool()
        {
        }

        public virtual void Activate(Vector3 directionToNextWaypoint)
        {
            Position = targetPoint.PosV3;
            Rotation = Quaternion.LookRotation(directionToNextWaypoint);
            unitGameObject.SetActive(true);
        }

        protected virtual void RigidbodyActivate()
        {
        }

        protected virtual void AddScore(int score)
        {
        }

        public void Rotate()
        {
            if (unitMonoBehavior.triggered)
                return;

            var dirToTarget = targetPoint.PosV2 - PositionV2;
            var forward = new Vector2(unitGameObject.transform.forward.x, unitGameObject.transform.forward.z);
            if ((forward.normalized - dirToTarget.normalized).magnitude > 0.1f)
            {
                unitGameObject.transform.Rotate(Vector3.Cross(unitGameObject.transform.forward, new Vector3(dirToTarget.x, 0, dirToTarget.y)), turnSpeed);
            }
        }

        protected virtual void RotateWheels()
        {
            if (wheels == null) return;
            foreach (var wheel in wheels)
            {
                wheel.Rotate(speed);
            }
        }

        //private float t;
        //private float startSpeed;
        //private float limit = 3;
        private void ChangeSpeed()
        {
            //if (t < limit)
            //    t += Time.deltaTime;
            Speed = rigidbody.velocity.magnitude;
            //Speed = startSpeed * (t - limit) * (t - limit)/limit/limit;
        }

        protected void Explode()
        {
            var exploder = unitGameObject.GetComponent<Exploder>();
            if (exploder != null && !exploder.enabled)
            {
                var explosion = new Explosion(Position, exploder);
                Explosions.ExplosionsList.Add(explosion);
            }
        }

        public Transform FindChild(string childName)
        {
            return unitGameObject.transform.Find(childName);
        }

        public void Destroy()
        {
            Object.Destroy(unitGameObject);
        }
    }
}
