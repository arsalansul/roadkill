﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Car : Unit
    {
        public Car(GameObject prefab) : base(prefab)
        {
        }

        public void CreateGO(Vector3 startPosition, Quaternion startRotation, bool active)
        {
            CreateUnitGO(startPosition,startRotation,active);
            unitMonoBehavior.rigidbodyActivate += RigidbodyActivate;
            unitMonoBehavior.explosionActivate += Exploded;
            unitMonoBehavior.addScore += AddScore;
        }

        private void Exploded()
        {
            ScoreController.AddScoreForExplosion(this);
        }

        protected override void AddScore(int score)
        {
            ScoreController.AddScoreForCollision(score);
        }

        public override void Activate(Vector3 directionToNextWaypoint)
        {
            Position = targetPoint.PosV3;
            Rotation = Quaternion.LookRotation(directionToNextWaypoint);
            Speed = Settings.Instance.unitSettings.Speed;
            unitGameObject.SetActive(true);
        }

        public override void ReturnToPool()
        {
            unitMonoBehavior.triggered = false;
            unitMonoBehavior.Reset();
            
            Position = new Vector3(1000, 1000, 1000);
            unitGameObject.GetComponent<Exploder>().enabled = false;
            unitGameObject.GetComponent<Exploder>().Reset();

            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = new Vector3(0f, 0f, 0f);

            unitGameObject.GetComponentInChildren<Renderer>().material.color = Color.white;

            unitGameObject.SetActive(false);
        }
    }
}
