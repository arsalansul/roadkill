﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class WaypointsManager
    {
        private Waypoint[] waypoints;
        private List<TrafficLight> trafficLights;
        public Transform waypointsParent { get; }

        public int Level { get; }

        public WaypointsManager(Transform waypointsParent, int level)
        {
            Level = level;

            waypoints = new Waypoint[waypointsParent.childCount];
            trafficLights = new List<TrafficLight>();

            for (int i = 0; i < waypoints.Length; i++)
            {
                waypoints[i] = new Waypoint
                {
                    PosV3 = waypointsParent.GetChild(i).position,
                    index = i,
                    IsFree = true,
                    canMove = true
                };

                if (waypointsParent.GetChild(i).childCount != 0)
                {
                    var TrafficLightParent = new GameObject("TrafficLightParent");
                    TrafficLightParent.transform.position = waypointsParent.GetChild(i).GetChild(0).position;
                    TrafficLightParent.transform.rotation = waypointsParent.GetChild(i).GetChild(0).rotation;

                    var TrafficControllParent =new GameObject("TrafficControllParent");
                    TrafficControllParent.transform.position = waypointsParent.GetChild(i).GetChild(1).position;
                    TrafficControllParent.transform.rotation = waypointsParent.GetChild(i).GetChild(1).rotation;

                    trafficLights.Add(new TrafficLight(TrafficLight.LightColor.Red, TrafficLightParent.transform, TrafficControllParent.transform, waypoints[i-1], waypoints[i]));
                }
            }

            waypointsParent.gameObject.SetActive(false);
            this.waypointsParent = waypointsParent;
        }

        public void OnReached(Unit unit, Waypoint wp)
        {
            if (unit.targetPoint.index + 1 < waypoints.Length)
            {
                unit.targetPoint = waypoints[unit.targetPoint.index + 1];
                return;
            }

            unit.targetPoint = waypoints[0];
            unit.ReturnToPool();
        }

        public Waypoint GetStart()
        {
            return waypoints[0];
        }

        public Vector3 GetDirectionToNextWaypoint(Waypoint current)
        {
            var nextIndex = current.index + 1;
            if (nextIndex == waypoints.Length)
                return Vector3.zero;

            return waypoints[nextIndex].PosV3 - current.PosV3;
        }

        public void ResetWaipoints()
        {
            for (int i = 0; i < waypoints.Length; i++)
            {
                waypoints[i].alwaysBusy = false;
                waypoints[i].IsFree = true;
                waypoints[i].canMove = true;

                foreach (var trafficLight in trafficLights)
                {
                    trafficLight.Reset();
                }
            }
        }
    }
}
