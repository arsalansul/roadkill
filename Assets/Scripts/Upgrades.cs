﻿using UnityEngine;

namespace Assets.Scripts
{
    class Upgrade
    {
        public readonly int startCost;

        private readonly string description;
        public string Description => "<color=#" + textColorHex + ">" + Name + "</color> " + Level + "\n" + description;

        public string Name { get; }
        private int level;
        public int Level => level;

        private Color32 textColor;
        public string textColorHex { get; }

        public bool forAD;

        public Upgrade(int cost, string description, Color32 textColor, string name, int startLevel)
        {
            startCost = cost;
            this.description = description;
            this.textColor = textColor;
            textColorHex = textColor.r.ToString("X2") + textColor.g.ToString("X2") + textColor.b.ToString("X2");
            Name = name;
            level = startLevel;
        }

        //public override string ToString()
        //{
        //    return description + " \nCurrent level " + level + "\nUpgrade cost " + Cost;
        //}

        public void IncreaseLevel()
        {
            level++;
        }
    }

    public class Upgrades
    {
        private Upgrade ExplosionUpgrade;
        private Upgrade CollisionUpgrade;
        private Upgrade OfflineUpgrade;
        private Upgrade DamageUpgrade;

        private Upgrade currentUpgrade;

        public string Description  => currentUpgrade.Description;
        public string TextColorHex  => currentUpgrade.textColorHex;

        private int cost;

        public System.Action NeedUpdateView;

        public int Cost
        {
            get
            {
                if (currentUpgrade.forAD)
                    return 0;

                return cost;
            }
        }

        public int Level  => currentUpgrade.Level;
        public string Name => currentUpgrade.Name;

        public string CostText
        {
            get
            {
                if (currentUpgrade.forAD)
                    return "For AD";

                return "$" + Cost;
            }
        }

        public Upgrades(int explosionStartLevel, int collisionStartLevel, int damageStartLevel, int offlineStartLevel)
        {
            ExplosionUpgrade = new Upgrade(1000, "More explosions - more money", new Color32(150, 43, 4, 255), "Explosion reward", explosionStartLevel);
            SetExplosionMultiplier();
            CollisionUpgrade = new Upgrade(1000, "More collisions - more money", new Color32(6, 92, 173, 255), "Collision reward", collisionStartLevel);
            SetCollisionMultiplier();
            OfflineUpgrade = new Upgrade(1000, "", new Color32(226, 158, 12, 255), "Offline earnings", offlineStartLevel);
            SetOfflineMultiplier();
            DamageUpgrade = new Upgrade(1000, "More damage - more explosions", new Color32(96, 43, 152, 255), "Damage", damageStartLevel);
            SetDamageMultiplier();

            currentUpgrade = ExplosionUpgrade;
            cost = GetExplosionUpgradeCost();
        }

        public void GetExplosionUpgrade()
        {
            currentUpgrade = ExplosionUpgrade;
            cost = GetExplosionUpgradeCost();
        }

        public void GetCollisionUpgrade()
        {
            currentUpgrade = CollisionUpgrade;
            cost = GetCollisionUpgradeCost();
        }

        public void GetOfflineUpgrade()
        {
            currentUpgrade = OfflineUpgrade;
            cost = GetOfflineUpgradeCost();
        }

        public void GetDamageUpgrade()
        {
            currentUpgrade = DamageUpgrade;
            cost = GetDamageUpgradeCost();
        }

        public bool UpgradeAccess()
        {
            return Settings.Instance.Money >= Cost;
        }

        public void Upgrade()
        {
            if (currentUpgrade.forAD)
            {
                AdsController.Instance().ShowAd(AdsProvider.AdType.unskipablevideo, ForAD);
                return;
            }

            currentUpgrade.IncreaseLevel();
            Settings.Instance.Money -= Cost;

            if (currentUpgrade == ExplosionUpgrade)
            {
                SetExplosionMultiplier();
                cost = GetExplosionUpgradeCost();
            }
            else if (currentUpgrade == CollisionUpgrade)
            {
                SetCollisionMultiplier();
                cost = GetCollisionUpgradeCost();
            }
            else if (currentUpgrade == DamageUpgrade)
            {
                SetDamageMultiplier();
                cost = GetDamageUpgradeCost();
            }
            else if (currentUpgrade == OfflineUpgrade)
            {
                SetOfflineMultiplier();
                cost = GetOfflineUpgradeCost();
            }
        }

        public int GetExplosionUpgradeLevel()
        {
            return ExplosionUpgrade.Level;
        }
        public int GetCollisionUpgradeLevel()
        {
            return CollisionUpgrade.Level;
        }
        public int GetOfflineUpgradeLevel()
        {
            return OfflineUpgrade.Level;
        }
        public int GetDamageUpgradeLevel()
        {
            return DamageUpgrade.Level;
        }

        private void SetExplosionMultiplier()
        {
            Settings.scoreMultiplierExplosion = 1 + ExplosionUpgrade.Level * ExplosionUpgrade.Level / 100f;
        }

        private void SetCollisionMultiplier()
        {
            Settings.scoreMultiplierCollision = 1 + Mathf.Pow(CollisionUpgrade.Level, 1.75f) / 150f;
        }

        private void SetDamageMultiplier()
        {
            Settings.damageMultiplier = 1 + DamageUpgrade.Level * DamageUpgrade.Level / 100f;
        }

        private void SetOfflineMultiplier()
        {
            Settings.scoreMultiplierOffline = 1 + OfflineUpgrade.Level * OfflineUpgrade.Level / 100f;
        }

        public int GetExplosionUpgradeCost()
        {
            return ExplosionUpgrade.startCost * ExplosionUpgrade.Level;
        }
        public int GetCollisionUpgradeCost()
        {
            return CollisionUpgrade.startCost * CollisionUpgrade.Level;
        }
        public int GetOfflineUpgradeCost()
        {
            return OfflineUpgrade.startCost * OfflineUpgrade.Level;
        }
        public int GetDamageUpgradeCost()
        {
            return DamageUpgrade.startCost * DamageUpgrade.Level * DamageUpgrade.Level;
        }

        public void GetRandomUpgradeForAD()
        {
            var i = Random.Range(0, 4);
            switch (i)
            {
                case 0:
                    GetExplosionUpgrade();
                    break;
                case 1:
                    GetCollisionUpgrade();
                    break;
                case 2:
                    GetDamageUpgrade();
                    break;
                case 3:
                    GetOfflineUpgrade();
                    break;
            }

            currentUpgrade.forAD = true;
        }

        public void ResetADState()
        {
            ExplosionUpgrade.forAD = false;
            CollisionUpgrade.forAD = false;
            OfflineUpgrade.forAD = false;
            DamageUpgrade.forAD = false;
        }

        private void ForAD(bool complete)
        {
            if (!complete)
            {
                return;
            }

            currentUpgrade.forAD = false;
            currentUpgrade.IncreaseLevel();

            if (currentUpgrade == ExplosionUpgrade)
            {
                SetExplosionMultiplier();
                cost = GetExplosionUpgradeCost();
            }
            else if (currentUpgrade == CollisionUpgrade)
            {
                SetCollisionMultiplier();
                cost = GetCollisionUpgradeCost();
            }
            else if (currentUpgrade == DamageUpgrade)
            {
                SetDamageMultiplier();
                cost = GetDamageUpgradeCost();
            }
            else if (currentUpgrade == OfflineUpgrade)
            {
                SetOfflineMultiplier();
                cost = GetOfflineUpgradeCost();
            }

            NeedUpdateView();
        }
    }
}
