﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    public class ObjectPooler
    {
        public List<Unit> units;
        private readonly GameObject[] carsPrefabs = Resources.LoadAll<GameObject>("Prefabs/Cars/");
        private UnitMonoBehavior[] _carsPrefabsUnitMonoBehavior;

        private readonly GameObject[] helicoptersPrefabs = Resources.LoadAll<GameObject>("Prefabs/Helicopters/");
        private UnitMonoBehavior[] _helicoptersPrefabsUnitMonoBehavior;

        //Создает для каждого префаба из папки "Prefabs/Cars/" экземпляры в кол-ве count
        public void CreateUnitsPool(int count)
        {
            if (units == null)
                units = new List<Unit>();

            _carsPrefabsUnitMonoBehavior = new UnitMonoBehavior[carsPrefabs.Length];

            for (int i = 0; i < carsPrefabs.Length; i++)
            {
                _carsPrefabsUnitMonoBehavior[i] = carsPrefabs[i].GetComponent<UnitMonoBehavior>();
                for (int j = 0; j < count; j++)
                {
                    CreateCar(carsPrefabs[i]);
                }
            }

            _helicoptersPrefabsUnitMonoBehavior = new UnitMonoBehavior[helicoptersPrefabs.Length];

            for (int i =0; i < helicoptersPrefabs.Length; i++)
            {
                _helicoptersPrefabsUnitMonoBehavior[i] = helicoptersPrefabs[i].GetComponent<UnitMonoBehavior>();
                for (int j = 0; j < count; j++)
                {
                    CreateHelicopter(helicoptersPrefabs[i]);
                }
            }

        }

        public void ClearPool()
        {
            for (int i = 0; i < units.Count; i++)
            {
                units[i].Destroy();
            }

            units = null;
        }

        public void ReturnAllUnitsToPool()
        {
            for (int i = 0; i < units.Count; i++)
            {
                units[i].ReturnToPool();
            }

            if (_helicoptersPrefabsUnitMonoBehavior.Min(unit => unit.levels[0]) < Levels.Level)
                Settings.Instance.unitsProbability.Helicopter = Mathf.Min(Settings.Instance.unitsProbability.Helicopter+1, Settings.Instance.unitsProbability.Car/3);
        }

        private Unit CreateCar(GameObject carGO)
        {
            var car = new Car(carGO);
            //car.onReached = onReached;
            //car.StartWaypoint = startWaypoint;
            //car.targetPoint = startWaypoint;
            units.Add(car);
            car.CreateGO(Vector3.forward, Quaternion.identity, false);

            return car;
        }

        private Unit CreateHelicopter(GameObject helicopterGO)
        {
            var helicopter = new Helicopter(helicopterGO);
            //helicopter.onReached = onReached;
            //helicopter.StartWaypoint = startWaypoint;
            //helicopter.targetPoint = startWaypoint;
            units.Add(helicopter);
            helicopter.CreateGO(Vector3.forward, Quaternion.identity, false);

            return helicopter;
        }

        private Unit PickUnitFromPoolByName(string name)
        {
            foreach (var unit in units)
            {
                if (unit.IsFree() && unit.Name == name)
                {
                    return unit;
                }
            }

            foreach (var car in carsPrefabs)
            {
                if (car.name == name)
                {
                    return CreateCar(car);
                }
            }

            foreach (var helicopter in helicoptersPrefabs)
            {
                if (helicopter.name == name)
                {
                    return CreateHelicopter(helicopter);
                }
            }

            Debug.LogError("Can't find unit by name " + name);
            return null;
        }

        private Unit PickUnitFromPoolByPrefab(GameObject prefab)
        {
            return PickUnitFromPoolByName(prefab.name);
        }

        public void ActivateRandomUnit(Waypoint startWaypoint, Vector3 dirToNextWaypoint, Action<Unit, Waypoint> OnReached, int level)
        {
            if (startWaypoint.IsFree)
            {
                var carsCount = _carsPrefabsUnitMonoBehavior.Count(unit => unit.levels.Length == 0 || unit.levels.Contains(level) || (unit.andGreater && unit.levels[unit.levels.Length - 1] < level));
                var helicoptersCount = _helicoptersPrefabsUnitMonoBehavior.Count(unit => unit.levels.Length == 0 || unit.levels.Contains(level) || (unit.andGreater && unit.levels[unit.levels.Length - 1] < level));

                var ind = Random.Range(0, carsCount * Settings.Instance.unitsProbability.Car + helicoptersCount * Settings.Instance.unitsProbability.Helicopter);
                GameObject[] unitGO;
                if (ind < carsCount * Settings.Instance.unitsProbability.Car)
                {
                    unitGO = new GameObject[1];
                    unitGO[0] = carsPrefabs[ind / Settings.Instance.unitsProbability.Car];
                }
                else
                {
                    unitGO = new GameObject[2];
                    unitGO[0] = helicoptersPrefabs[(ind - carsCount * Settings.Instance.unitsProbability.Car) / Settings.Instance.unitsProbability.Helicopter];

                    ind = Random.Range(0, carsPrefabs.Length);
                    unitGO[1] = carsPrefabs[ind];
                }

                foreach (var go in unitGO)
                {
                    var unit = PickUnitFromPoolByPrefab(go);
                    unit.StartWaypoint = startWaypoint;
                    unit.targetPoint = startWaypoint;
                    unit.onReached = OnReached;

                    unit?.Activate(dirToNextWaypoint);
                }
            }
        }

        
    }
}
