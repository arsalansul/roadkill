﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class Tutorials
    {
        private Transform tutorialCanvas;
        private GameObject fireTutorialImage;
        private GameObject upgradeTutorialImage;

        public Tutorials()
        {
            tutorialCanvas = GameObject.Find("TutorialCanvas").transform;
            fireTutorialImage = tutorialCanvas.Find("Fire").gameObject;
            upgradeTutorialImage = tutorialCanvas.Find("Upgrade").gameObject;
        }

        public void FireTutorialActivate()
        {
            fireTutorialImage.SetActive(true);
        }

        public void FireTutorialDeactivate()
        {
            fireTutorialImage.SetActive(false);
            ProgressSaver.FireTutorialComplete();
        }

        public void UpgradeTutorialActivate()
        {
            upgradeTutorialImage.SetActive(true);
        }

        public void UpgradeTutorialDeactivate()
        {
            upgradeTutorialImage.SetActive(false);
            ProgressSaver.UpgradeTutorialComplete();
        }
    }
}
