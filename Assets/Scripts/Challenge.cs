﻿namespace Assets.Scripts
{
    public class Challenge
    {
        public string Description
        {
            get
            {
                string result;
                if (explosionsCount != 0)
                {
                    result = "BLOW UP " + explosionsCount + " ";
                    if (TargetName != null)
                        result += TargetName;
                    else
                    {
                        result += "CARS";
                    }

                    return result;
                }

                if (causeDamage != 0)
                {
                    return "CAUSE " + causeDamage + " DAMAGE";
                }

                return null;
            }
            set { }
        }

        public int explosionsCount;
        public int explodedCount;
        public string TargetName;

        public int causeDamage;
        public int causedDamage;

        public bool complete;

        public void Reset()
        {
            explodedCount = 0;
            causedDamage = 0;
        }
    }
}
