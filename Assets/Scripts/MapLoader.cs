﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Helpers;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    public class MapLoader
    {
        private List<GameObject> mapGameObjects;
        private GameObject[] prefabs;
        private const string prefabPath = "Prefabs/Map/";

        private int[] indexes;
        
        private Camera camera;

        public bool needCheckMaps;
        public MapLoader(Camera camera)
        {
            mapGameObjects = new List<GameObject>();
            this.camera = camera;

            prefabs = Resources.LoadAll<GameObject>(prefabPath);
            indexes = new int[prefabs.Length];
            for (int i = 0; i < prefabs.Length; i++)
            {
                indexes[i] = prefabs[i].GetComponent<MapLoaderHelper>().index;
            }
        }

        public bool CheckMapsToRenderOnScreen()
        {
            if (mapGameObjects.Count != 0)
            {
                for (int i = 0; i < mapGameObjects.Count; i++)
                {
                    var start = mapGameObjects[i].transform.Find("start");
                    var end = mapGameObjects[i].transform.Find("end");

                    Vector3 screenStartPoint = camera.WorldToViewportPoint(start.position);
                    bool startOnScreen = screenStartPoint.z > 0 && screenStartPoint.x > 0 && screenStartPoint.x < 1 && screenStartPoint.y > 0 && screenStartPoint.y < 1;

                    Vector3 screenEndPoint = camera.WorldToViewportPoint(end.position);
                    bool endOnScreen = screenEndPoint.z > 0 && screenEndPoint.x > 0 && screenEndPoint.x < 1 && screenEndPoint.y > 0 && screenEndPoint.y < 1;

                    if (!startOnScreen && !endOnScreen && (screenStartPoint.y * screenStartPoint.z < 0 && screenEndPoint.y * screenEndPoint.z < 0 ||
                                                           screenStartPoint.y * screenStartPoint.z > 1 && screenEndPoint.y * screenEndPoint.z > 1))
                    {
                        mapGameObjects[i].SetActive(false);
                    }
                    else
                    {
                        mapGameObjects[i].SetActive(true);
                    }

                    if (i == mapGameObjects.Count - 1 && endOnScreen)
                    {
                        return true;
                    }
                }
            }

            needCheckMaps = false;
            return false;
        }

        public void LoadMap(out GameObject newMapGO)
        {
            var goIndex = indexes[Random.Range(0, indexes.Length)];
            LoadMap(goIndex, out newMapGO);

            ProgressSaver.SaveMap(newMapGO.GetComponent<MapLoaderHelper>().index);
        }

        private void LoadMap(int index, out GameObject newMapGO)
        {
            var go = prefabs[Array.BinarySearch(indexes, index)];
            
            Vector3 lastEndPosition;
            if (mapGameObjects.Count > 0)
                lastEndPosition = mapGameObjects[mapGameObjects.Count - 1].transform.Find("end").position;
            else
            {
                lastEndPosition = Vector3.zero;
            }

            newMapGO = Object.Instantiate(go, new Vector3(lastEndPosition.x, 0, 0), Quaternion.identity);
            var start = newMapGO.transform.Find("start");

            newMapGO.transform.position += lastEndPosition - start.position;
            mapGameObjects.Add(newMapGO);
        }

        public void LoadStartMap(out List<GameObject> newMapGOs)
        {
            var startMap = ProgressSaver.GetMapIndexes();
            newMapGOs = new List<GameObject>();

            for (int i = 0; i < startMap.Length; i++)
            {
                if (startMap[i] == 0)
                    continue;

                LoadMap(startMap[i], out var go);
                newMapGOs.Add(go);
            }
        }
    }
}
