﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class Arrow
    {
        private Transform arrowDummyGameObject;
        private Transform arrowGameObject;

        private const float speed = 200;
        private const float amplitude = 100;
        public Arrow(Transform arrowDummy)
        {
            arrowDummyGameObject = arrowDummy;
            arrowGameObject = arrowDummy.Find("Arrow");
        }

        private int direction = 1;

        public void Rotate()
        {
            if (!arrowDummyGameObject.gameObject.activeInHierarchy)
                return;

            var angle = GetAngle(arrowGameObject.localEulerAngles.y);
            angle += speed * direction * Time.deltaTime;

            if (angle > amplitude/2 || angle < -amplitude/2)
            {
                angle = amplitude / 2 * direction;
                direction *= -1;
            }
            arrowGameObject.transform.localEulerAngles = new Vector3(0, angle, 0);
        }

        private float GetAngle(float angle) // => [-180;180]
        {
            while (angle >= 180)
            {
                angle -= 360;
            }

            while (angle <= -180)
            {
                angle += 360;
            }

            return angle;
        }

        private Vector3 GetAngle(Vector3 vector)
        {
            return new Vector3(vector.x, GetAngle(vector.y), vector.z);
        }

        public Quaternion GetRotation()
        {
            return arrowGameObject.localRotation;
        }

        public void Disable()
        {
            arrowDummyGameObject.gameObject.SetActive(false);
        }
    }
}
