﻿using UnityEngine;

namespace Assets.Scripts
{
    public class GameObjectKiller : MonoBehaviour
    {
        private float timer = 0;
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
            timer += Time.deltaTime;
            if (timer > 3)
            {
                Destroy(gameObject);
            }
        }
    }
}
