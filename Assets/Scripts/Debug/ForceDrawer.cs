﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceDrawer : MonoBehaviour
{
    public Vector3 point;
    public Vector3 force;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.TransformPoint(point), transform.TransformPoint(point + force));
    }
}
