﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.UI;

public class HealthSettings : MonoBehaviour
{
    private Slider slider;

    private Text label;

    // Start is called before the first frame update
    void Start()
    {
        slider = GetComponent<Slider>();
        slider.minValue = 0.5f;
        slider.maxValue = 2;
        slider.value = Settings.Instance.unitSettings.HealthMultiplier;

        label = transform.Find("Text").GetComponent<Text>();
        label.text += " " + slider.value;

        slider.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
    }

    // Update is called once per frame
    void Update()
    {
        Settings.Instance.unitSettings.HealthMultiplier = (int)slider.value;
    }

    private void ValueChangeCheck()
    {
        label.text = "Health " + slider.value;
    }
}
