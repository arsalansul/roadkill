﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.UI;

public class PowerSettings : MonoBehaviour
{
    private Slider slider;

    private Text label;
    // Start is called before the first frame update
    void Start()
    {
        slider = GetComponent<Slider>();
        slider.maxValue = 100;
        slider.value = Settings.Instance.explosionSettings.Power;

        label = transform.Find("Text").GetComponent<Text>();
        label.text += " " + slider.value;
        slider.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
    }

    // Update is called once per frame
    void Update()
    {
        Settings.Instance.explosionSettings.Power = slider.value;
    }

    private void ValueChangeCheck()
    {
        label.text = "Power " + slider.value;
    }
}
