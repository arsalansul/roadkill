﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Settings : MonoBehaviour
    {
        [System.Serializable]
        public class ExplosionSettings
        {
            [Range(0, 100)] public int Radius;
            [Range(0f, 100f)] public float Power;
            public bool ExplosionDebugRender;
        }

        [System.Serializable]
        public class UnitSettings
        {
            [Range(0, 100)] public float Speed;
            [Range(0, 100)] public float RangeToSpawn;
            [Range(0.5f, 2f)] public float HealthMultiplier;
        }

        [System.Serializable]
        public class UnitsProbability
        {
            [Range(0, 100)] public int Car;
            [Range(0, 100)] public int Helicopter;
        }

        [System.Serializable]
        public class Challenges
        {
            public int causeDamage;
        }

        public static Settings Instance { get; private set; }

        public ExplosionSettings explosionSettings;
        public UnitSettings unitSettings;
        public UnitsProbability unitsProbability;
        public Challenges[] challenges;

        public static UIStates uiState;

        public static float scoreMultiplierExplosion = 1;

        public static float scoreMultiplierCollision
        {
            get
            {
                if (scoreMultiplierCollisionHelper)
                    return collisionMultiplier * 1.5f;

                return collisionMultiplier;
            }

            set => collisionMultiplier = value;
        }

        public static float damageMultiplier = 1;
        public static float scoreMultiplierOffline = 1;
        public static int MaxScoreOffline = 1000;

        private static float collisionMultiplier = 1;
        public static bool scoreMultiplierCollisionHelper;

        private static int money = 0;
        public int Money
        {
            get => money;
            set => money = value > 0 ? value : 0;
        }
        private void Awake()
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
}
