﻿using UnityEngine;

namespace Assets.Scripts
{
    public class GiftManager
    {
        private int counter;

        private const int counterUpgradeForAD = 1;
        private const int counterCollisionMult = 3;

        private int currentLevel;
        private Upgrades upgrades;
        private UIController uiController;

        public GiftManager(Upgrades upgrades, UIController uiController)
        {
            this.upgrades = upgrades;
            currentLevel = Levels.Level;
            this.uiController = uiController;
        }

        public void CheckForGifts()
        {
            if (Levels.Level > currentLevel)
            {
                counter = 0;
                currentLevel = Levels.Level;
                return;
            }

            counter++;
            if (counter % counterUpgradeForAD == 0)
            {
                var gift = new Gift(delegate
                {
                    upgrades.GetRandomUpgradeForAD();
                    uiController.UpgradeViewUpdate();
                });
                gift.Show();
            }

            if (counter % counterCollisionMult == 0)
            {
                var gift = new Gift(delegate { Settings.scoreMultiplierCollisionHelper = true;});
                gift.Show();
            }
        }
    }
}
