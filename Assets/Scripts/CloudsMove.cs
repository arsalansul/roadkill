﻿using UnityEngine;

public class CloudsMove : MonoBehaviour
{
    private const float speed = 5;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, transform.position + transform.forward, Time.deltaTime * speed);
        if (Mathf.Abs(transform.position.z) > 300)
            transform.position = new Vector3 (transform.position.x, transform.position.y,  Mathf.Clamp(- transform.position.z, -299f, 299f));
    }
}
