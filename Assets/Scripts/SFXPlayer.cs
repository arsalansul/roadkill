﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SFXPlayer : MonoSingleton<SFXPlayer> {

	private List<MyAudioSource> players = new List<MyAudioSource>();

	private int currPlayer;

	private bool isOn = true;

	private List<float> volumes = new List<float>();

    private List<AudioClip> justPlayed = new List<AudioClip>();
    private List<float> timers = new List<float>();

	//[ResetOnReloadAttribute]
	private static bool inited;

	// Use this for initialization
	void Start () {

		if (!inited)
			Init ();
	}

    private void Update()
    {
        for(int i = 0; i < justPlayed.Count; i++)
        {
            timers[i] -= Time.deltaTime;
            if(timers[i] <= 0)
            {
                justPlayed.RemoveAt(i);
                timers.RemoveAt(i);
                i--;
            }
        }
    }

    protected override void Init ()
	{
        _volume = 1f;

        base.Init ();

		DontDestroyOnLoad (gameObject);

		players.Clear ();
		volumes.Clear ();

		AddPlayer (1);
		currPlayer = 0;

		Debug.Log ("Init SFX Player " + PlayerPrefs.GetInt ("sfxOffOn", 0));

		if (PlayerPrefs.GetInt ("sfxOffOn", 0) == 1) {

			Off ();
		}

		inited = true;
	}

	private void AddPlayer(float volume) {

		GameObject go = new GameObject ("Player" + players.Count);
		go.transform.SetParent (transform);
		MyAudioSource player = go.AddComponent<MyAudioSource> ();
		players.Add (player);
		volumes.Add (volume);

		if (isOn) {
			player.volume = volume * _volume;
		} else {
			player.volume = 0;
		}
	}

	public MyAudioSource Play(AudioClip clip, bool repeat = false, bool samePlayer = false, float volume = 1) {

		if (!inited)
			Init ();

        float v = _volume;
        if (v == 0) v = 0.01f;
        if (!samePlayer && justPlayed.Contains(clip))
        {
            for (int p = 0; p < players.Count; p++)
            {
                if (players[p].clip == clip && players[p].volume / v > 0.1f) return null;
            }
        }

        justPlayed.Add(clip);
        timers.Add(0.1f);

		//Debug.Log ("Play " + clip + " " + repeat + " " + samePlayer + " " + volume);

		if (samePlayer) {

			//Debug.Log ("Searching player for " + clip);

			for (int p = 0; p < players.Count; p++) {
				if (players [p].clip == clip) {
					//Debug.Log ("Found at " + p);
					players [p].Stop ();
					players[p].Play();
					if (isOn) players [p].volume = volume * _volume;
					else players [p].volume = 0;
					volumes [p] = volume;
					return players[p];
				}
				//Debug.Log ("Not found");
			}
		}

		int i = currPlayer;
		while(i >= 0 && i < players.Count && players[i].isPlaying) {

			i++;
			i = i % players.Count;
			if(i == currPlayer) {
				AddPlayer(volume);
				i = players.Count-1;
				break;
			}
		}

		currPlayer = i;

		players [currPlayer].loop = repeat;
		players [currPlayer].clip = clip;
		players [currPlayer].Play ();
		if (isOn) players [currPlayer].volume = volume * _volume;
		else players [currPlayer].volume = 0;
		volumes [currPlayer] = volume;

		return players [currPlayer];
	}

	public void Stop(AudioClip clip) {

		for (int i = 0; i < players.Count; i++)
			if (players [i].clip == clip)
				players [i].Stop ();
	}

	public void Off() {

		isOn = false;

		PlayerPrefs.SetInt ("sfxOffOn", 1);

		for (int i = 0; i < players.Count; i++) {
			players [i].volume = 0;
		}
	}

	public void On() {

		isOn = true;

		PlayerPrefs.SetInt ("sfxOffOn", 0);

		for (int i = 0; i < players.Count; i++) {
			players [i].volume = volumes[i] * _volume;
		}

	}

	public bool Switch() {

		isOn = !isOn;

		if (isOn) {
			On ();
			return true;
		} else {
			Off ();
			return false;
		}
	}

	public bool IsPlaying(AudioClip clip) {

		if (clip == null)
			return false;

		for (int i = 0; i < players.Count; i++)
			if (players [i].clip == clip && players[i].isPlaying)
				return true;

		return false;
	}

    private float _volume;
    public float volume { get { return _volume; } set { _volume = value; if (_volume < 0) _volume = 0; if (_volume > 1) _volume = 1; } }
}
