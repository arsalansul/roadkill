﻿using UnityEngine;

namespace Assets.Scripts
{
    public class TrafficLight
    {
        private GameObject trafficLightGameObject = Resources.Load<GameObject>("Prefabs/TrafficLight");
        private GameObject trafficControllerGameObject = Resources.Load<GameObject>("Prefabs/ExplodableBuildings/TrafficController");
        //private Transform trafficLightParent = GameObject.Find("TrafficLightParent").transform;

        private const string shaderName = "Standard";

        private Waypoint waypointBegin;
        private Waypoint waypointEnd;

        private ExplodableBuildings TrafficController;
        public enum LightColor
        {
            Red,
            Yellow,
            Green
        }

        public LightColor lightColor {get ;private set;}

        //public System.Action GreenLight;
        public TrafficLight(LightColor lightColor, Transform TrafficLightParent, Transform TrafficControllParent, Waypoint waypointBeforTrafficLight, Waypoint waypointNext)
        {
            trafficLightGameObject = Object.Instantiate(trafficLightGameObject, TrafficLightParent);
            this.lightColor = lightColor;

            waypointBegin = waypointBeforTrafficLight;
            waypointEnd = waypointNext;

            SetLight(this.lightColor);

            TrafficController = new ExplodableBuildings(trafficControllerGameObject, this);
            TrafficController.Create(TrafficControllParent);
        }

        public void SetLight(LightColor lc)
        {
            lightColor = lc;
            Color result = Color.white;
            switch (lightColor)
            {
                case LightColor.Red:
                    result = Color.red;
                    waypointEnd.canMove = false;
                    waypointBegin.alwaysBusy = true;
                    break;
            
                case LightColor.Yellow:
                    result = Color.yellow;
                    break;
            
                case LightColor.Green:
                    result = Color.green;
                    waypointEnd.canMove = true;
                    break;
            }
            trafficLightGameObject.transform.GetChild(0).GetComponent<Light>().color = result;
        }

        public void Reset()
        {
            TrafficController.Reset();
            SetLight(LightColor.Red);
        }
    }
}
