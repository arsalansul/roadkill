﻿using UnityEngine;

namespace Assets.Scripts
{
    [ExecuteAlways]
    public class WaypointsEditor : MonoBehaviour
    {
        public bool AddWaypoint;

        public Color color;

        private const string shaderName = "Standard";

        public bool AddTrafficLight;

        // Start is called before the first frame update
        void Start()
        {
            foreach (Transform waypoint in transform)
            {
                var tempMaterial = new Material(Shader.Find(shaderName));
                tempMaterial.color = color;
                waypoint.GetComponent<MeshRenderer>().material = tempMaterial;

                if (waypoint.childCount > 0)
                {
                    foreach (Transform child in waypoint)
                    {
                        child.GetComponent<MeshRenderer>().material = tempMaterial;
                    }
                }
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (AddTrafficLight)
            {
                CreateTrafficControllPoint();
            }

            if (AddWaypoint)
            {
                CreateWaypoint(); 
            }

            AddWaypoint = false;
            AddTrafficLight = false;
        }

        private void CreateWaypoint()
        {
            var waypoint = GameObject.CreatePrimitive(PrimitiveType.Cube);
            waypoint.name = "waipoint";
            waypoint.transform.SetParent(transform);

            var tempMaterial = new Material(Shader.Find(shaderName));
            tempMaterial.color = color;
            waypoint.GetComponent<MeshRenderer>().material = tempMaterial;
        }

        private void CreateTrafficControllPoint()
        {
            var waypoint = GameObject.CreatePrimitive(PrimitiveType.Cube);
            waypoint.name = "waipoint";
            waypoint.transform.SetParent(transform);

            var TrafficLightParent = GameObject.CreatePrimitive(PrimitiveType.Capsule);
            TrafficLightParent.name = "TrafficLightParent";
            TrafficLightParent.transform.SetParent(waypoint.transform);

            var trafficControll = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            trafficControll.name = "TrafficControllParent";
            trafficControll.transform.SetParent(waypoint.transform);

            var tempMaterial = new Material(Shader.Find(shaderName));
            tempMaterial.color = color;
            waypoint.GetComponent<MeshRenderer>().material = tempMaterial;
            trafficControll.GetComponent<MeshRenderer>().material = tempMaterial;
            trafficControll.GetComponent<MeshRenderer>().material = tempMaterial;
        }
    }
}
