﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace Assets.Scripts
{
    public class UIController
    {
        private Canvas GameCanvas;
        private Canvas RewardCanvas;

        private Button FireButton;

        private Slider challengeSlider;
        private Text challengeText;

        private Transform upgradeButtonsParent;
        private Animator upgradePanelAnimator;

        private Button explosionUpgradeButton;
        private Button collisionUpgradeButton;
        private Button offlineUpgradeButton;
        private Button damageUpgradeButton;
        private Button upgradeButton;
        private Transform upgradeButtonImagesParent;
        private Text upgradeDescriptionText;
        private Text upgradeCostText;

        private Text currentLVLText;
        private Text nextLVLText;

        private Upgrades upgrades;

        private Text moneyText;

        private Button ClaimRewardButton;
        private Button ClaimRewardForAdButton;
        private Text RewardDescriptionText;
        private Text RewardAmountText;

        private Transform NextLevelWindow;
        private Text NextLevelWindow_text;

        private Animator rewardAnimator;

        private GameObject ExplosionBonusText_GameObject = Resources.Load<GameObject>("Prefabs/UI/ExplosionBonusText");

        private Button resetButton;

        private Tutorials tutorials;

        private Reward reward;

        public UIController(Upgrades upgrades)
        {
            GameCanvas = GameObject.Find("GameCanvas").GetComponent<Canvas>();
            RewardCanvas = GameObject.Find("RewardCanvas").GetComponent<Canvas>();

            RewardCanvas.gameObject.SetActive(false);

            tutorials = new Tutorials();

            var Head = GameCanvas.transform.Find("Head");

            FireButton = GameCanvas.transform.Find("FireButton").GetComponent<Button>();

            challengeSlider = Head.Find("Challenge").GetComponent<Slider>();
            challengeText = Head.Find("Description").Find("Text").GetComponent<Text>();

            upgradeButtonsParent = GameCanvas.transform.Find("Upgrades");
            explosionUpgradeButton = upgradeButtonsParent.Find("ExplosionUpgrade").GetComponent<Button>();
            collisionUpgradeButton = upgradeButtonsParent.Find("CollisionUpgrade").GetComponent<Button>();
            offlineUpgradeButton = upgradeButtonsParent.Find("OfflineUpgrade").GetComponent<Button>();
            damageUpgradeButton = upgradeButtonsParent.Find("DamageUpgrade").GetComponent<Button>();

            upgradeButton = upgradeButtonsParent.Find("UpgradeView").Find("Upgrade").GetComponent<Button>();
            upgradeDescriptionText = upgradeButtonsParent.Find("UpgradeView").Find("Description").GetComponent<Text>();
            upgradeCostText = upgradeButton.transform.Find("Cost").GetComponent<Text>();
            upgradeButtonImagesParent = upgradeButton.transform.Find("Images");
            upgradePanelAnimator = upgradeButtonsParent.GetComponent<Animator>();

            var RewardPanel = RewardCanvas.transform.Find("RewardPanel");
            ClaimRewardButton = RewardPanel.Find("ClaimButton").GetComponent<Button>();
            ClaimRewardForAdButton = RewardPanel.Find("ClaimForAdButton").GetComponent<Button>();

            RewardDescriptionText = RewardPanel.Find("Label").GetComponent<Text>();
            RewardAmountText = RewardPanel.Find("Amount").GetComponent<Text>();

            this.upgrades = upgrades;

            explosionUpgradeButton.onClick.AddListener(upgrades.GetExplosionUpgrade);
            explosionUpgradeButton.onClick.AddListener(UpgradeViewUpdate);

            collisionUpgradeButton.onClick.AddListener(upgrades.GetCollisionUpgrade);
            collisionUpgradeButton.onClick.AddListener(UpgradeViewUpdate);

            offlineUpgradeButton.onClick.AddListener(upgrades.GetOfflineUpgrade);
            offlineUpgradeButton.onClick.AddListener(UpgradeViewUpdate);

            damageUpgradeButton.onClick.AddListener(upgrades.GetDamageUpgrade);
            damageUpgradeButton.onClick.AddListener(UpgradeViewUpdate);

            upgradeButton.onClick.AddListener(upgrades.Upgrade);
            upgradeButton.onClick.AddListener(UpgradeViewUpdate);
            upgrades.NeedUpdateView += UpgradeViewUpdate;

            moneyText = Head.Find("Money").GetComponent<Text>();

            currentLVLText = Head.Find("CurrentLVL").Find("Text").GetComponent<Text>();
            nextLVLText = Head.Find("NextLVL").Find("Text").GetComponent<Text>();

            NextLevelWindow = GameCanvas.transform.Find("NextLevelWindow");
            NextLevelWindow_text = NextLevelWindow.Find("Text").GetComponent<Text>();

            rewardAnimator = RewardCanvas.gameObject.GetComponent<Animator>();

            ScoreController.explosionText = ExplosionBonusText;

            UpgradeViewUpdate();

            resetButton = GameCanvas.transform.Find("Reset").GetComponent<Button>();
            resetButton.onClick.AddListener(Reset);
        }

        public void UpdateUI()
        {
            switch (Settings.uiState)
            {
                case UIStates.Game:
                    GameCanvas.gameObject.SetActive(true);
                    currentLVLText.text = Levels.Level.ToString();
                    nextLVLText.text = (Levels.Level + 1).ToString();
                    RewardCanvas.gameObject.SetActive(false);
                    NextLevelWindow.gameObject.SetActive(false);
                    EnableButtons();
                    break;
                case UIStates.Menu:
                    GameCanvas.gameObject.SetActive(false);
                    break;
                case UIStates.LevelSelect:
                    break;
            }
            CheckTutorials();
        }
        
        public void DisableButtons()
        {
            FireButton.gameObject.SetActive(false);
            upgradePanelAnimator.SetInteger("State", 1);

            upgrades.ResetADState();
        }

        private void EnableButtons()
        {
            FireButton.gameObject.SetActive(true);
            upgradePanelAnimator.SetInteger("State", 2);
        }

        public void SetStartChallengeValues(Challenge challenge)
        {
            challengeSlider.maxValue = Mathf.Max(challenge.explosionsCount, challenge.causeDamage);
            challengeSlider.value = 0;
            challengeText.text = challenge.Description;
        }

        public void SetChallengeValues(Challenge challenge)
        {
            if (challenge.explosionsCount != 0)
                challengeSlider.value = challenge.explodedCount;
            else
            {
                challengeSlider.value = challenge.causedDamage;
                challengeText.text = challenge.causedDamage + "/" + challenge.causeDamage;
            }

            if (challenge.complete)
            {
                challengeText.text = "COMPLETE";
                challengeSlider.value = challengeSlider.maxValue;
            }
        }

        public void UpgradeViewUpdate()
        {
            if (!AdsController.Instance().IsAdReady(AdsProvider.AdType.unskipablevideo))
                upgrades.ResetADState();

            upgradeDescriptionText.text =upgrades.Description;
            upgradeCostText.text = upgrades.CostText;
            moneyText.text = "$" + Settings.Instance.Money;

            foreach (Transform upgradeButtonImage in upgradeButtonImagesParent)
            {
                upgradeButtonImage.gameObject.SetActive(false);
                if (upgradeButtonImage.name == upgrades.Name && upgrades.UpgradeAccess())
                {
                    upgradeButtonImage.gameObject.SetActive(true);
                    upgradeButton.targetGraphic = upgradeButtonImage.GetComponent<Image>();
                }
            }

            if (upgrades.UpgradeAccess())
            {
                upgradeButton.interactable = true;
            }
            else
            {
                upgradeButton.interactable = false;
                upgradeButtonImagesParent.GetChild(upgradeButtonImagesParent.childCount - 1).gameObject.SetActive(true);
            }

            ChangeButtonsColor();
        }

        public void ShowReward(Reward reward, Color color)
        {
            this.reward = reward;

            RewardDescriptionText.text = reward.description;
            RewardAmountText.text = "$" + reward.amount;

            RewardCanvas.transform.Find("RewardPanel").GetComponent<Image>().color = color;
            RewardCanvas.gameObject.SetActive(true);

            rewardAnimator.SetInteger("State", 1);

            ClaimRewardButton.onClick.RemoveAllListeners();
            ClaimRewardButton.onClick.AddListener(reward.Claim);
            ClaimRewardButton.onClick.AddListener(UpgradeViewUpdate);
            ClaimRewardButton.onClick.AddListener(delegate { rewardAnimator.SetInteger("State", 0); });
            ClaimRewardButton.onClick.AddListener(delegate { RewardCanvas.gameObject.SetActive(false); });
            ClaimRewardButton.onClick.AddListener(() => ProgressSaver.Save(upgrades));
            
            ClaimRewardForAdButton.transform.Find("Text").GetComponent<Text>().text = $"x{Reward.GetMultForAD()} ${reward.amount * Reward.GetMultForAD()}";

            if (AdsController.Instance().IsAdReady(AdsProvider.AdType.unskipablevideo))
            {
                ClaimRewardForAdButton.onClick.AddListener(() =>
                    AdsController.Instance().ShowAd(AdsProvider.AdType.unskipablevideo, RewardUnskipableADComplete));
                ClaimRewardForAdButton.enabled = true;
                ClaimRewardForAdButton.GetComponent<Image>().color = Color.white;
            }
            else
            {
                ClaimRewardForAdButton.enabled = false;
                ClaimRewardForAdButton.GetComponent<Image>().color = Color.grey;
                ClaimRewardForAdButton.transform.Find("Text").GetComponent<Text>().text = String.Empty;
            }
        }

        public void ShowReward(Reward reward, UnityAction buttonAction)
        {
            var color = Levels.comlete ? Color.white : (Color)new Color32(162,162,162, 255);
            ShowReward(reward, color);

            ClaimRewardButton.onClick.AddListener(buttonAction);

            if (AdsController.Instance().IsAdReady(AdsProvider.AdType.unskipablevideo))
            {
                ClaimRewardForAdButton.onClick.RemoveAllListeners();
                ClaimRewardForAdButton.onClick.AddListener(() =>
                    AdsController.Instance().ShowAd(AdsProvider.AdType.unskipablevideo, RewardUnskipableADComplete));
                ClaimRewardForAdButton.onClick.AddListener(buttonAction);
            }
            else
            {
                ClaimRewardForAdButton.enabled = false;
                ClaimRewardForAdButton.GetComponent<Image>().color = Color.gray;
                ClaimRewardForAdButton.transform.Find("Text").GetComponent<Text>().text = String.Empty;
            }
        }

        public void ChangeButtonsColor()
        {
            Color color = new Color32(200,200,200,255);

            //TODO rework hacks
            explosionUpgradeButton.transform.GetComponent<Image>().color = upgrades.Name == "Explosion reward" ? Color.white : color;
            collisionUpgradeButton.transform.GetComponent<Image>().color = upgrades.Name == "Collision reward" ? Color.white : color;
            offlineUpgradeButton.transform.GetComponent<Image>().color = upgrades.Name == "Offline earnings" ? Color.white : color;
            damageUpgradeButton.transform.GetComponent<Image>().color = upgrades.Name == "Damage" ? Color.white : color;
        }

        public void NextLevelWindow_show()
        {
            NextLevelWindow_text.text = (Levels.Level).ToString();
            NextLevelWindow.gameObject.SetActive(true);
        }

        private void ExplosionBonusText(int bonus, Vector3 positionWorld)
        {
            var positionViewport = Camera.main.WorldToViewportPoint(positionWorld);
            positionViewport.x = Mathf.Clamp(positionViewport.x, 0.1f, 0.9f);
            positionViewport.y = Mathf.Clamp(positionViewport.y, 0.1f, 0.9f);

            var position = Camera.main.ViewportToScreenPoint(positionViewport);
            var instance = Object.Instantiate(ExplosionBonusText_GameObject, position,Quaternion.identity, GameCanvas.transform);

            instance.GetComponent<Text>().text = "Explosion\n+" + bonus;
        }

        public void CheckTutorials()
        {
            if (Levels.Level == 1 && ProgressSaver.GetFireTutorialStatus() == 0)
            {
                tutorials.FireTutorialActivate();
                FireButton.onClick.AddListener(tutorials.FireTutorialDeactivate);
                FireButton.onClick.AddListener(FireButtonRemoveTutorialListener);
            }
            else if (Levels.Level == 2 && ProgressSaver.GetUpgradeTutorialStatus() == 0)
            {
                tutorials.UpgradeTutorialActivate();
                FireButton.gameObject.SetActive(false);
                upgradeButton.onClick.AddListener(tutorials.UpgradeTutorialDeactivate);
                upgradeButton.onClick.AddListener(UpgradeButtonRemoveTutorialListener);
            }
        }

        private void FireButtonRemoveTutorialListener()
        {
            FireButton.onClick.RemoveListener(tutorials.FireTutorialDeactivate);
            FireButton.onClick.RemoveListener(FireButtonRemoveTutorialListener);
        }

        private void UpgradeButtonRemoveTutorialListener()
        {
            FireButton.gameObject.SetActive(true);
            upgradeButton.onClick.RemoveListener(tutorials.UpgradeTutorialDeactivate);
            upgradeButton.onClick.RemoveListener(UpgradeButtonRemoveTutorialListener);
        }

        private void Reset()
        {
            ProgressSaver.LoseProgress();
            Object.Destroy(Settings.Instance.gameObject);
            SceneManager.LoadScene(0, LoadSceneMode.Single);
        }

        private void RewardUnskipableADComplete(bool complete)
        {
            if (!complete)
                return;

            reward.ClaimForAd();
            UpgradeViewUpdate();
            rewardAnimator.SetInteger("State", 0);
            RewardCanvas.gameObject.SetActive(false);
            ProgressSaver.Save(upgrades);
        }
    }
}
