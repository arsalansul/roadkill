﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class Wheel
    {
        public GameObject wheelGameObject;

        public Wheel(GameObject wheelGameObject)
        {
            this.wheelGameObject = wheelGameObject;
        }

        public void Rotate(float speed)
        {
            wheelGameObject.transform.Rotate(new Vector3(0.3f*speed, 0, 0));
        }

        public void Rotate(Vector3 speed)
        {
            wheelGameObject.transform.Rotate(speed);
        }
    }
}
