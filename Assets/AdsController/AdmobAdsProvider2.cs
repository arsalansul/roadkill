﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using admob;

public class AdmobAdsProvider2 : AdsProvider {

    public string appId;

    public string rating = "G";

	// These ad units are configured to always serve test ads.
	#if UNITY_ANDROID
	public string bannerId = "ca-app-pub-3940256099942544/6300978111";
	#elif UNITY_IPHONE
	public string bannerId = "ca-app-pub-3940256099942544/2934735716";
	#else
	public string bannerId = "unexpected_platform";
	#endif

	// These ad units are configured to always serve test ads.
	#if UNITY_ANDROID
	public string interstitialId = "ca-app-pub-3940256099942544/1033173712";
	#elif UNITY_IPHONE
	public string interstitialId = "ca-app-pub-3940256099942544/4411468910";
	#else
	public string interstitialId = "unexpected_platform";
	#endif

	#if UNITY_ANDROID
	public string rewardedVideoId = "ca-app-pub-3940256099942544/5224354917";
	#elif UNITY_IPHONE
	public string rewardedVideoId = "ca-app-pub-3940256099942544/1712485313";
	#else
	public string rewardedVideoId = "unexpected_platform";
	#endif

	//private Action<bool> onFinish;

	private bool showing;

#if !UNITY_EDITOR
	private bool bannerLoaded;
#endif

    private bool requestBanner;

	private bool requestInterstitial;

	private bool requestRewardedVideo;

	private bool shouldShowBanner;

	[SerializeField]private bool bannerAtTop = false;
	[SerializeField]private bool smartBanner = false;

	public bool useBanner = true;

    private Admob admob;

    public override void Init() {

		if (adsSettings != null) {

			bannerId = adsSettings.GetValue<string> ("ADMOBBANNERID", bannerId);
			interstitialId = adsSettings.GetValue<string> ("ADMOBINTERSTITIALID", interstitialId);
			rewardedVideoId = adsSettings.GetValue<string> ("ADMOBREWARDEDVIDEOID", rewardedVideoId);
			bannerAtTop = adsSettings.GetValue<bool> ("BANNERATTOP", bannerAtTop);
			smartBanner = adsSettings.GetValue<bool> ("SMARTBANNER", smartBanner);
			chance = adsSettings.GetValue<float> (name + "CHANCE", chance);
		}

        AdProperties adProperties = new AdProperties();
        adProperties.maxAdContentRating(rating);

        admob = Admob.Instance();
        admob.bannerEventHandler += onBannerEvent;
        admob.interstitialEventHandler += onInterstitialEvent;
        admob.rewardedVideoEventHandler += onRewardedVideoEvent;
        admob.initSDK(adProperties);//reqired,adProperties can been null

        if (useBanner) RequestBanner();

		//RequestInterstitial ();

		//RequestRewardBasedVideo ();

		update += Update;

		inited = true;
	}

    void onInterstitialEvent(string eventName, string msg)
    {
        Debug.Log("handler onAdmobEvent---" + eventName + "   " + msg);
        if (eventName == AdmobEvent.onAdLoaded) HandleInterstitialLoaded();
        else if (eventName == AdmobEvent.onAdFailedToLoad) HandleInterstitialFailedToLoad();
        else if (eventName == AdmobEvent.onAdOpened) HandleInterstitialOpened();
        else if (eventName == AdmobEvent.onAdClosed) HandleInterstitialClosed();
    }
    void onBannerEvent(string eventName, string msg)
    {
        Debug.Log("handler onAdmobBannerEvent---" + eventName + "   " + msg);
        if (eventName == AdmobEvent.onAdLoaded) HandleAdLoaded();
        else if (eventName == AdmobEvent.onAdFailedToLoad) HandleAdFailedToLoad();
        else if (eventName == AdmobEvent.onAdOpened) HandleAdOpened();
        else if (eventName == AdmobEvent.onAdClosed) HandleAdClosed();
    }
    void onRewardedVideoEvent(string eventName, string msg)
    {
        Debug.Log("handler onRewardedVideoEvent---" + eventName + "  rewarded: " + msg);
        if (eventName == AdmobEvent.onAdLoaded) HandleRewardBasedVideoLoaded();
        else if (eventName == AdmobEvent.onAdFailedToLoad) HandleRewardBasedVideoFailedToLoad();
        else if (eventName == AdmobEvent.onAdOpened) HandleRewardBasedVideoOpened();
        else if (eventName == AdmobEvent.onAdClosed) HandleRewardBasedVideoClosed();
    }


    public override bool IsBannerShowing ()
	{
		return shouldShowBanner;
	}

	public override void HideBanner ()
	{
		shouldShowBanner = false;
        if (admob != null) admob.removeAllBanner();

    }

	private void RequestBanner()
	{
        Debug.Log("Request banner admob2");
        requestBanner = false;

        if(admob != null) admob.showBannerRelative(bannerId, AdSize.SMART_BANNER, AdPosition.BOTTOM_CENTER);
    }

	private void RequestInterstitial()
	{
		//Logger.Instance().Log ("Request interstitial");

		requestInterstitial = false;

		if(admob != null) admob.loadInterstitial(interstitialId);
    }

	private void RequestRewardBasedVideo()
	{
        //Logger.Instance().Log("Request rewarded");

        requestRewardedVideo = false;

        if (admob != null) admob.loadRewardedVideo(rewardedVideoId);
	}

	public override void ShowAd(AdType type, Action<bool> onFinish = null) {

		if (!inited)
		{
			Init();
			if (!inited) return;
		}

		if (type == AdType.banner) {
			shouldShowBanner = true;
			
            return;
		}

		if (showing)
			return;

        StartCoroutine(ShowAdCoroutine(type, onFinish));
	}

    private IEnumerator ShowAdCoroutine (AdType type, Action<bool> onFinish = null)
    {
        while(waitingForAdEnd) yield return null;

        waitingForAdEnd = true;
        result = false;

        if (type == AdType.interstitial || type == AdType.skipablevideo)
        {
            if (admob.isInterstitialReady())
            {
                showing = true;
                admob.showInterstitial();
            }
        }

        if (type == AdType.unskipablevideo)
        {
            if (admob.isRewardedVideoReady())
            {
                showing = true;
                admob.showRewardedVideo();
            }
        }

        while (waitingForAdEnd)
        {
            //Logger.Instance().Log("waiting");
            yield return null;
        }

        //Logger.Instance().Log("Finished with " + result);

        if (onFinish != null) onFinish(result);
    }

	public override void HideAd(AdType type) {

		if (type == AdType.banner) {
			shouldShowBanner = false;
            if (admob != null) admob.removeAllBanner();  
		}
	}

	public override bool IsAdReady(AdType type) {

		if (showing)
			return false;

        if (admob == null) return false;

		#if UNITY_EDITOR
		return false;
#else
		if (type == AdType.banner) return bannerLoaded;
		if (type == AdType.interstitial) return admob.isInterstitialReady();
		if (type == AdType.skipablevideo) return admob.isInterstitialReady();
		if (type == AdType.unskipablevideo) return admob.isRewardedVideoReady();
		return false;
#endif
    }

    private float requestBannerTimer = 2f;
    private float requestInterstitialTimer = 2;
	private float requestRewardedVideoTimer = 2;

	//protected override void _Update () {
	void Update() {

        if (requestBanner) {
            requestBannerTimer -= Time.deltaTime;
            if (requestBannerTimer <= 0)
                RequestBanner ();
        }

        if (requestInterstitial) {
			requestInterstitialTimer -= Time.deltaTime;
			if (requestInterstitialTimer <= 0)
				RequestInterstitial ();
		}

		if (requestRewardedVideo) {
			requestRewardedVideoTimer -= Time.deltaTime;
			if (requestRewardedVideoTimer <= 0)
				RequestRewardBasedVideo ();
		}
	}

	void OnDestroy() {

	}

	#region Banner callback handlers

	private void HandleAdLoaded()
	{
#if !UNITY_EDITOR
		bannerLoaded = true;
#endif

        Logger.Instance().Log("Banner loaded");
    }

    private void HandleAdFailedToLoad()
	{
        requestBanner = true;
        requestBannerTimer = 2f;

#if !UNITY_EDITOR
		bannerLoaded = false;
#endif
        Logger.Instance().Log ("Banner failed");
	}

	private void HandleAdOpened()
	{
        //Logger.Instance().Log ("HandleAdOpened event received");
	}

	private void HandleAdClosed()
	{
#if !UNITY_EDITOR
		bannerLoaded = false;
#endif
        //Logger.Instance().Log ("HandleAdClosed event received");
	}

	private void HandleAdLeftApplication()
	{
		//print("HandleAdLeftApplication event received");
	}

	#endregion

	#region Interstitial callback handlers

	private void HandleInterstitialLoaded()
	{
		//Logger.Instance().Log ("HandleInterstitialLoaded event received");
	}

	private void HandleInterstitialFailedToLoad()
	{
		requestInterstitial = true;
		requestInterstitialTimer = 2;
		//Logger.Instance().Log ("HandleInterstitialFailedToLoad event received with message: " + args.Message);
	}

	private void HandleInterstitialOpened()
	{
		//Logger.Instance().Log ("HandleInterstitialOpened event received");
	}

	private void HandleInterstitialClosed()
	{
		showing = false;
		requestInterstitial = true;
        //Logger.Instance().Log("HandleInterstitialClosed event received " + requestInterstitial);

        //AudioListener.volume = storedVolume;
        //Time.timeScale = storedTScale;

        waitingForAdEnd = false;
        result = true;

		//if(this.onFinish != null) this.onFinish(true);
	}

	private void HandleInterstitialLeftApplication()
	{
		//Logger.Instance().Log ("HandleInterstitialLeftApplication event received");
	}

	#endregion

	#region RewardBasedVideo callback handlers

	private bool waitingForAdEnd;
    private bool result;
	//private float storedVolume;
	//private float storedTScale;

	private void HandleRewardBasedVideoLoaded()
	{
		//Logger.Instance().Log ("HandleRewardBasedVideoLoaded event received");
	}

	private void HandleRewardBasedVideoFailedToLoad()
	{
		requestRewardedVideo = true;
		requestRewardedVideoTimer = 2;
		//Logger.Instance().Log ("HandleRewardBasedVideoFailedToLoad event received with message: " + args.Message);
	}

	private void HandleRewardBasedVideoOpened()
	{
		//Logger.Instance().Log ("HandleRewardBasedVideoOpened event received");
	}

	private void HandleRewardBasedVideoStarted()
	{
		//Logger.Instance().Log ("HandleRewardBasedVideoStarted event received");
	}

	private void HandleRewardBasedVideoClosed()
	{
		showing = false;
		requestRewardedVideo = true;
		//Logger.Instance().Log ("HandleRewardBasedVideoClosed event received");

		//if (!waitingForAdEnd) return;

		//AudioListener.volume = storedVolume;
		//Time.timeScale = storedTScale;

		waitingForAdEnd = false;
        result = false || result;

		//if(this.onFinish != null) this.onFinish(false);
	}

	private void HandleRewardBasedVideoRewarded()
	{
        showing = false;
		//Logger.Instance().Log ("HandleRewardBasedVideoRewarded event received for " + amount.ToString() + " " + type);

		//if (!waitingForAdEnd) return;

		waitingForAdEnd = false;
        result = true;

		//AudioListener.volume = storedVolume;
		//Time.timeScale = storedTScale;

		//if(this.onFinish != null) this.onFinish(true);
	}

	private void HandleRewardBasedVideoLeftApplication()
	{
		//Logger.Instance().Log ("HandleRewardBasedVideoLeftApplication event received");
	}

	#endregion
}
