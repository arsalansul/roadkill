﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Logger : MonoBehaviour {

	public Text console;

	private static Logger instance;

	public static Logger Instance () {
	
		if (instance == null) {
			instance = FindObjectOfType<Logger> ();
		}

		if (instance == null) {
		
			GameObject go = new GameObject ("Logger");
			instance = go.AddComponent<Logger> ();
			try
			{
				go = GameObject.FindGameObjectWithTag ("Console");
				if(go != null) instance.console = go.GetComponent<Text>();
			} catch {
			}
		}

		return instance;
	}
		
	public void Log(string s) {
	
		if (console == null) {
			try
			{
				GameObject go = GameObject.FindGameObjectWithTag ("Console");
				if (go != null) console = go.GetComponent<Text> ();
			} catch {
			}
		}

		if (console != null)
			console.text += s + "\n";

		Debug.Log (s);
	}
	
}
