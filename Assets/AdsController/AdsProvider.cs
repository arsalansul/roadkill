﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdsProvider : MonoBehaviour {

	public string provider;

    public Action onBannerLoaded;

	public Action update;

	public float chance;

	public bool onOdd;
	public bool onEven;

	public enum AdType {
		banner,
		interstitial,
		skipablevideo,
		unskipablevideo,
	}

	protected bool inited;

	protected AdsSettings adsSettings;

	public void SetSettings (AdsSettings settings) {
	
		adsSettings = settings;
	}

	public virtual void Init() {
	
		inited = true;
	}

	public virtual void ShowAd(AdType type, Action<bool> onFinish = null) {
	
		if (!inited) {
			Init ();
			if(!inited) return;
		}
	}

    public virtual void HideAd(AdType type) {

    }

	public virtual bool IsAdReady(AdType type) {
	
		return false;
	}

	public virtual bool IsBannerShowing() {
	
		return false;
	}

	public virtual void HideBanner () {
	
	}
}
